# frozen_string_literal: true

# Test the Space class.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative '../spec_helper'

describe Travails::Space do
  let(:space) { described_class.new([0, 0]) }

  describe '#initialize' do
    context 'when providing no arguments' do
      it 'raises an error' do
        expect { described_class.new }.to raise_error(ArgumentError)
      end
    end

    context 'when providing the positional argument' do
      it 'sets the coords attribute to the given value' do
        expect(space).to have_attributes(coords: [0, 0])
      end

      it 'sets the paths attribute to an empty array' do
        expect(space).to have_attributes(paths: [])
      end
    end
  end

  describe '#inspect' do
    it 'returns the coordinates of the node in a readable format' do
      expect(space.inspect).to match(/(\d+,\s\d+)/)
    end
  end
end
