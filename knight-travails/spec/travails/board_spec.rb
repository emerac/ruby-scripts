# frozen_string_literal: true

# Test the Board class.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative '../spec_helper'

describe Travails::Board do
  let(:board) { described_class.new }

  describe '#initialize' do
    context 'when not provided an argument' do
      it 'creates the board with the default number of spaces' do
        expect(board.spaces.length).to eq(64)
      end

      it 'populates each index with a space' do
        board.spaces.each { |space| expect(space).not_to be_nil }
      end
    end

    context 'when provided with a side length' do
      let(:side_length) { 4 }

      let(:board) { described_class.new(side_length) }

      it 'creates the board with a custom number of spaces' do
        expect(board.spaces.length).to eq(side_length * side_length)
      end

      it 'populates each index with a space' do
        board.spaces.each { |space| expect(space).not_to be_nil }
      end
    end
  end

  describe '#move_knight' do
    context 'when the path requires two coords' do
      it 'returns the path' do
        expect(board.move_knight([0, 0], [1, 2]).length).to eq(2)
      end
    end

    context 'when the path requires three coords' do
      it 'returns the path when starting in a corner' do
        expect(board.move_knight([0, 0], [3, 3]).length).to eq(3)
      end

      it 'returns the path when ending in a corner' do
        expect(board.move_knight([3, 3], [0, 0]).length).to eq(3)
      end
    end

    context 'when the path requires four coords' do
      it 'returns the path' do
        expect(board.move_knight([3, 3], [4, 3]).length).to eq(4)
      end
    end

    context 'when the path requires seven coords' do
      it 'returns the path' do
        expect(board.move_knight([0, 0], [7, 7]).length).to eq(7)
      end
    end
  end
end
