# frozen_string_literal: true

# Represent a space on a chessboard.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

module Travails
  # Represents a knight's position on a chessboard with each valid move
  # being a path that it can take.
  class Space
    attr_accessor :coords, :paths

    def initialize(coords)
      @coords = coords
      @paths = []
    end

    def inspect
      "(#{coords.join(', ')})"
    end
  end
end
