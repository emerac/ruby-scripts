# frozen_string_literal: true

# A chess board.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

module Travails
  # Represents a chess board.
  class Board
    attr_reader :spaces

    def initialize(side_length = 8, space = Travails::Space)
      @side_length = side_length
      @spaces = create_spaces(space)
      connect_spaces
    end

    def move_knight(start_coords, end_coords)
      # Moves a knight from one set of coords to another.
      start_space = find_space(start_coords)
      end_space = find_space(end_coords)
      previous_spaces = map_previous_spaces(start_space)
      reconstruct_path(start_space, end_space, previous_spaces)
    end

    private

    def connect_spaces
      # For each space, adds nearby spaces that are valid knight moves.
      @spaces.each do |space|
        connected_coords = find_connected_coords(space.coords)
        connected_spaces = connected_coords.map { |coords| find_space(coords) }
        connected_spaces.each { |connect_space| space.paths << connect_space }
      end
    end

    def coords2index(coords)
      # Converts a set of coords to an index in a non-nested array.
      (coords[0] * @side_length) + coords[1]
    end

    def create_chessboard_array(contents = nil)
      # Returns an array containing an index for each chessboard space.
      Array.new(@side_length * @side_length, contents)
    end

    def create_spaces(space)
      # Returns an array of spaces that reflect chessboard coords.
      spaces = create_chessboard_array
      spaces.each_index { |i| spaces[i] = space.new(index2coords(i)) }
      spaces
    end

    def find_connected_coords(coords)
      # Returns an array of coords that represents valid knight moves.
      possible_coords = []
      possible_coords << [coords[0] + 1, coords[1] - 2]
      possible_coords << [coords[0] + 2, coords[1] - 1]
      possible_coords << [coords[0] + 2, coords[1] + 1]
      possible_coords << [coords[0] + 1, coords[1] + 2]
      possible_coords << [coords[0] - 1, coords[1] + 2]
      possible_coords << [coords[0] - 2, coords[1] + 1]
      possible_coords << [coords[0] - 2, coords[1] - 1]
      possible_coords << [coords[0] - 1, coords[1] - 2]
      possible_coords.filter { |coord| valid_move?(coord) }
    end

    def find_space(coords)
      # Returns the space at a set of coords.
      found_space = @spaces.filter { |space| space.coords == coords }
      found_space.empty? ? nil : found_space[0]
    end

    def index2coords(index)
      # Converts an index in a non-nested array to a set of coords.
      [index % @side_length, index / @side_length]
    end

    def reconstruct_path(start_space, end_space, previous_spaces)
      # Returns an array of coords leading from one space to another in
      # the shortest path possible.
      reverse_path = []
      current_space = end_space
      until reverse_path.include?(start_space)
        reverse_path.push(current_space)
        current_space = previous_spaces[coords2index(current_space.coords)]
      end
      reverse_path.reverse.map { |space| [space.coords[0], space.coords[1]] }
    end

    def map_previous_spaces(start_space)
      # Returns an array where each index contains the space that leads
      # to that index in a path starting at the given space.
      #
      # Perfoms a breadth-first search on the chessboard starting at the
      # given space and all spaces are visited once.
      queue = [start_space]
      discovered_spaces = [start_space]
      previous_spaces = create_chessboard_array

      until queue.empty?
        current_space = queue.shift
        current_space.paths.each do |path|
          next if discovered_spaces.include?(path)

          queue.push(path)
          discovered_spaces.push(path)
          previous_spaces[coords2index(path.coords)] = current_space
        end
      end
      previous_spaces
    end

    def valid_move?(move)
      # Returns a bool representing the validity of a move.
      move.all? { |n| n.between?(0, @side_length - 1) }
    end
  end
end
