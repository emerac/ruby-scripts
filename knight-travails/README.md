# knight travails

This program will find the shortest path required to move a knight on a
chessboard from one space to another.

This is done by performing a breadth-first search to find all paths
emanating from the starting position. The path is then reconstructed
from the search path.

## Usage

A console script has been provided, which allows you to experiment with
the program in an irb session. To use this script:

* clone the `ruby-scripts` repository
* `cd ruby-scripts/knight-travails`
* `bin/console`

If the last command produces a permission error, it is likely because
the script is not executable. You can change this with the command
`chmod u+x bin/console`.

## License

This program is licensed under the GNU General Public License. For
the full license text, view the [LICENSE](../LICENSE) file.
