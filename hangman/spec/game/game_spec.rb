# frozen_string_literal: true

# Test the Game class.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require 'json'
require 'stringio'

require_relative '../spec_helper'

describe Game do
  let(:setup) { {} }

  let(:game) { described_class.new(setup) }

  let(:game_file) do
    f = Tempfile.new('hangman_rspec_test')
    game_state = JSON.pretty_generate(
      {
        word: 'string',
        guesses_remaining: 2,
        correct_guesses: %w[a b],
        incorrect_guesses: %w[v w x y z]
      }
    )
    f.write(game_state)
    f.close
    f
  end

  let(:nonexist_path) do
    f = Tempfile.new('hangman_rspec_test')
    path = f.path
    f.close
    f.unlink
    path
  end

  describe '#inspect' do
    it 'returns information about the game object' do
      expect(game.inspect).to match(/[Hh]angman/)
    end
  end

  describe '#run_setup' do
    it 'outputs an introduction' do
      setup[:input] = StringIO.new('quit')
      regex = /[[:ascii:]]*Welcome[[:ascii:]]+/
      expect { game.run_setup(nonexist_path) }.to output(regex).to_stdout
    end

    it 'outputs the instructions' do
      setup[:input] = StringIO.new('quit')
      regex = /[[:ascii:]]*Try to guess[[:ascii:]]+/m
      expect { game.run_setup(nonexist_path) }.to output(regex).to_stdout
    end

    it 'outputs the license' do
      setup[:input] = StringIO.new('quit')
      regex = /[[:ascii:]]*Copyright.*GNU[[:ascii:]]+/m
      expect { game.run_setup(nonexist_path) }.to output(regex).to_stdout
    end

    context 'when there is no saved game found' do
      it 'does not ask to load a game' do
        setup[:input] = StringIO.new('quit')
        regex = /[[:ascii:]]*saved game found[[:ascii:]]+/m
        expect { game.run_setup(nonexist_path) }.not_to output(regex).to_stdout
      end
    end

    context 'when there is a saved game found' do
      it 'loads the game if the user answers affirmatively' do
        setup[:input] = StringIO.new("y\nquit")
        regex = /[[:ascii:]]*Saved game loaded[[:ascii:]]+/m
        path = game_file.path
        expect { game.run_setup(path) }.to output(regex).to_stdout

        game_file.unlink
      end

      it 'does not load the game if the user answers negatively' do
        setup[:input] = StringIO.new("n\nquit")
        regex = /[[:ascii:]]*Saved game not loaded[[:ascii:]]+/m
        path = game_file.path
        expect { game.run_setup(path) }.to output(regex).to_stdout

        game_file.unlink
      end

      it 're-prompts until a valid answer is given' do
        setup[:input] = StringIO.new("x\ny\nquit")
        regex = /[[:ascii:]]*Error.*Saved game loaded[[:ascii:]]+/m
        path = game_file.path
        expect { game.run_setup(path) }.to output(regex).to_stdout

        game_file.unlink
      end
    end
  end

  describe '#play' do
    let(:setup) { { output: StringIO.new } }

    it 'outputs a win message when the word is guessed' do
      setup[:word] = Word.new('string')
      setup[:input] = StringIO.new("s\nt\nr\ni\nn\ng")
      game.play
      expect(setup[:output].string).to match(/Congratulations!/m)
    end

    it 'outputs a lose message when the word is not guessed' do
      setup[:word] = Word.new('string')
      setup[:input] = StringIO.new("u\nv\nw\nx\ny\nz")
      game.play
      expect(setup[:output].string).to match(/Sorry!/m)
    end

    it 'outputs a notification when the game is quit' do
      setup[:input] = StringIO.new('quit')
      game.play
      expect(setup[:output].string).to match(/quit/m)
    end

    it 'outputs a notification when the game is saved' do
      setup[:input] = StringIO.new("save\nyes\nquit")
      game.play(nonexist_path)
      expect(setup[:output].string).to match(/saved/m)
    end

    it 'outputs a notification when the game is not saved' do
      setup[:input] = StringIO.new("save\nno\nquit")
      game.play(game_file.path)
      expect(setup[:output].string).to match(/not saved/m)

      game_file.unlink
    end

    it 're-prompts until a valid choice is made' do
      setup[:word] = Word.new('string')
      setup[:input] = StringIO.new("xyz\nquit")
      game.play
      expect(setup[:output].string).to match(/Error.*quit/m)
    end
  end

  describe '#save_game' do
    let(:setup) { { output: StringIO.new } }

    context 'when there is not a previously saved game' do
      it 'outputs a save successful notification' do
        game.save_game(nonexist_path)
        expect(setup[:output].string).to match(/Game saved/m)
      end
    end

    context 'when there is a previously saved game' do
      it 'prompts to overwrite and outputs a save succesful notification' do
        setup[:input] = StringIO.new('yes')
        game.save_game(game_file.path)
        expect(setup[:output].string).to match(/overwrite.*Game saved/m)

        game_file.unlink
      end

      it 'prompts to overwrite and outputs a save unsuccesful notification' do
        setup[:input] = StringIO.new('no')
        game.save_game(game_file.path)
        expect(setup[:output].string).to match(/overwrite.*Game not saved/m)

        game_file.unlink
      end

      it 're-prompts until a valid yes or no is given' do
        setup[:input] = StringIO.new("x\nno")
        game.save_game(game_file.path)
        expect(setup[:output].string).to match(/Error/m)

        game_file.unlink
      end
    end
  end
end
