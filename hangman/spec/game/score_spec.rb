# frozen_string_literal: true

# Test the Score class.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative '../spec_helper'

describe Score do
  let(:score) { described_class.new }

  describe '#initialize' do
    it 'creates the correct guesses variable with an empty array' do
      expect(score.correct_guesses).to eq([])
    end

    it 'creates the incorrect guesses variable with an empty array' do
      expect(score.incorrect_guesses).to eq([])
    end
  end

  describe '#add_correct_guess' do
    it 'adds the given char to the array containing correct guesses' do
      score.add_correct_guess('x')
      expect(score.correct_guesses).to include('x')
    end

    it 'raises an error when passing anything other than a single char' do
      expect { score.add_correct_guess('xyz') }.to raise_error(ArgumentError)
    end
  end

  describe '#add_incorrect_guess' do
    it 'adds the given char to the array containing incorrect guesses' do
      score.add_incorrect_guess('x')
      expect(score.incorrect_guesses).to include('x')
    end

    it 'raises an error when passing anything other than a single char' do
      expect { score.add_incorrect_guess('xyz') }.to raise_error(ArgumentError)
    end
  end

  describe '#form_correct_guesses' do
    it 'nicely formats all of the previous correct guesses' do
      regex = /\s*[Cc]orrect: \w, \w, \w$/
      score.add_correct_guess('x')
      score.add_correct_guess('y')
      score.add_correct_guess('z')
      expect(score.form_correct_guesses).to match(regex)
    end

    it 'returns no guesses when there are no previous correct guesses' do
      expect(score.form_correct_guesses).to match(/\s*[Cc]orrect:\s*$/)
    end
  end

  describe '#form_incorrect_guesses' do
    it 'nicely formats all of the previous incorrect guesses' do
      regex = /\s*[Ii]ncorrect: \w, \w, \w$/
      score.add_incorrect_guess('x')
      score.add_incorrect_guess('y')
      score.add_incorrect_guess('z')
      expect(score.form_incorrect_guesses).to match(regex)
    end

    it 'returns no guesses when there are no previous incorrect guesses' do
      expect(score.form_incorrect_guesses).to match(/\s*[Ii]ncorrect:\s*$/)
    end
  end

  describe '#form_guesses_remaining' do
    it 'returns a string describing the default number of guesses remaining' do
      expect(score.form_guesses_remaining).to match(/\s*[Tt]ries \w+: \d$/)
    end
  end
end
