# frozen_string_literal: true

# Test the Dictionary class.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require 'tempfile'

require_relative '../spec_helper'

describe Dictionary do
  let(:dict) { described_class.new(SpecHelper::DICT_PATH) }

  describe '#initialize' do
    it 'does not raise an error when the path exists and is not empty' do
      expect do
        described_class.new(SpecHelper::DICT_PATH)
      end.not_to raise_error
    end

    it 'raises an error when the path does not exist' do
      expect do
        described_class.new('nonexistent_path')
      end.to raise_error(ArgumentError)
    end

    it 'raises an error when the file is empty' do
      expect do
        tempfile = Tempfile.new('empty')
        described_class.new(tempfile.path)
      end.to raise_error(ArgumentError)
    end
  end

  describe '#inspect' do
    it 'returns a string describing the object and its path' do
      expect(dict.inspect).to match(/^#<.*:.*>$/)
    end
  end

  describe '#get_random_word' do
    it 'returns a random word whose length is between 5 and 7' do
      expect(dict.get_random_word(5, 7)).to match(/\w{5,7}/)
    end

    it 'returns a random word whose length is exactly 4' do
      expect(dict.get_random_word(4, 4).length).to eq(4)
    end

    it 'returns an empty string' do
      expect(dict.get_random_word(-2, 0)).to eq('')
    end

    it 'returns another word after having been used once already' do
      dict.get_random_word(5, 7)
      expect(dict.get_random_word(5, 7)).to match(/\w{5,7}/)
    end
  end
end
