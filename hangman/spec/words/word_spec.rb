# frozen_string_literal: true

# Test the Word class.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative '../spec_helper'

describe Word do
  let(:word) { described_class.new(string) }

  context 'when the length of the word is zero' do
    let(:string) { '' }

    describe '#initialize' do
      it 'creates the object to match the empty string' do
        expect(word.word).to eq(string)
      end
    end

    describe '#inspect' do
      it 'returns a string containing the class name and an empty string' do
        expect(word.inspect).to match(/^#<\w{1,}:>$/)
      end
    end

    describe '#form_word' do
      it 'returns an empty string when no visible chars are given' do
        expect(word.form_word([])).to eq(string)
      end

      it 'returns an empty string when two visible chars are given' do
        expect(word.form_word(%w[s i])).to eq(string)
      end
    end

    describe '#form_underline' do
      it 'returns an empty string' do
        expect(word.form_underline).to eq(string)
      end
    end
  end

  context 'when the length of the word is non-zero' do
    let(:string) { 'string' }

    describe '#initialize' do
      it 'creates the object to match the string' do
        expect(word.word).to eq(string)
      end
    end

    describe '#inspect' do
      it 'returns a string containing the class name and the given string' do
        expect(word.inspect).to match(/^#<\w{1,}:\w{1,}>$/)
      end
    end

    describe '#include?' do
      let(:string) { 'string' }

      it 'returns true when the word includes the provided char' do
        expect(word.include?('s')).to be(true)
      end

      it 'returns false when the word does not include the provided char' do
        expect(word.include?('x')).to be(false)
      end
    end

    describe '#form_word' do
      it 'returns the complete word when all chars are made visible' do
        expect(word.form_word(string.chars)).to match(/^\s*(\w\s*){5}\w$/)
      end

      it 'returns two chars when two chars are made visible' do
        expect(word.form_word(%w[s i])).to match(/^\s*(\w\s*){2}$/)
      end
    end

    describe '#form_underline' do
      it 'returns a number of underline chars to match the string length' do
        expect(word.form_underline).to match(/^\s*(─\s*){5}─$/)
      end
    end
  end
end
