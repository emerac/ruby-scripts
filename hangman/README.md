# hangman

Try to guess the word, character-by-character, before running out
of tries! Each correctly guessed character causes all matching
characters in the mystery word to be revealed. Each incorrect
guess lowers the amount of available tries by one.

## Usage

To play the game:

* clone the `ruby-scripts` repository
* `cd ruby-scripts/hangman`
* `bin/play`

If the last command produces a permission error, it is likely because
the script is not executable. You can change this with the command
`chmod u+x bin/play`.

## Notes

* By default, words between 5 and 12 characters long are randomly chosen
from a dictionary file.
* The dictionary file is located in the repository's top-level
directory.
* When a game is saved, the save file is placed in the repository's
top-level directory.

## License

This program is licensed under the GNU General Public License. For
the full license text, view the [LICENSE](../LICENSE) file.
