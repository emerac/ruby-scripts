# frozen_string_literal: true

# Contains classes related to the game.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require 'json'

# Wraps all of the in-game messages used to communicate with the user.
module Message
  def self.load_prompt
    "A saved game was found. Would you like to load it (y/n)?\n> "
  end

  def self.game_loaded
    'Saved game loaded!'
  end

  def self.game_lost(word)
    "Sorry! You lost! The word was '#{word}'."
  end

  def self.game_not_loaded
    'Saved game not loaded!'
  end

  def self.game_not_saved
    'Game not saved!'
  end

  def self.game_quit
    'Game quit!'
  end

  def self.game_saved
    'Game saved!'
  end

  def self.game_won
    'Congratulations! You won!'
  end

  def self.instructions
    <<~INSTRUCTIONS
      Try to guess the word, character-by-character, before running out
      of tries! Each correctly guessed character causes all matching
      characters in the mystery word to be revealed. Each incorrect
      guess lowers the amount of available tries by one.
    INSTRUCTIONS
  end

  def self.introduction
    'Welcome to the classic hangman game!'
  end

  def self.license
    <<~LICENSE
      hangman Copyright (C) 2021 emerac
      This program comes with ABSOLUTELY NO WARRANTY; This is free
      software, and you are welcome to redistribute it under certain
      conditions; You should have received a copy of the GNU General
      Public License along with this program.  If not, see
      <https://www.gnu.org/licenses/>.
    LICENSE
  end

  def self.overwrite_prompt
    "A saved game already exists. Would you like to overwrite it (y/n)?\n> "
  end

  def self.turn_error
    <<~ERROR
      Error: expected either: a single, not-previously-guessed,
      alphabetic character, the word 'save', or the word 'quit'
    ERROR
  end

  def self.turn_prompt
    "Enter a guess, 'save', 'quit':\n> "
  end

  def self.yes_no_error_message
    'Error: expected either \'y\' or \'n\''
  end
end

# Wraps all of the methods used for loading a game.
# The load_game method is the primary 'public' method for this module.
module LoadedGame
  include Message

  def load_game(path)
    save_data = parse_json(read_file(path))
    load_word_data(save_data)
    load_score_data(save_data)
  end

  def load_score_data(data)
    @score = Score.new
    data['correct_guesses'].each { |e| @score.add_correct_guess(e) }
    data['incorrect_guesses'].each { |e| @score.add_incorrect_guess(e) }
  end

  def load_word_data(data)
    @word = Word.new(data['word'])
  end

  def obtain_load_choice
    @output.print Message.load_prompt
    load_choice = @input.gets.chomp.downcase
    until valid_yes_no?(load_choice)
      @output.puts Message.yes_no_error_message
      @output.print Message.load_prompt
      load_choice = @input.gets.chomp.downcase
    end
    load_choice
  end

  def parse_json(data)
    JSON.parse(data)
  end

  def read_file(path)
    File.read(path)
  end
end

# Wraps all of the methods used for saving a game.
# The save_game method is the primary 'public' method for this module.
module SavedGame
  include Message

  def save_game(path = HangmanHelper::SAVE_PATH)
    if save_exist?(path) && %w[no n].include?(obtain_overwrite_choice)
      @output.puts Message.game_not_saved
    else
      write_to_file(path)
      @output.puts Message.game_saved
    end
  end

  def game_state
    JSON.pretty_generate(
      {
        word: @word.word,
        guesses_remaining: @score.guesses_remaining,
        correct_guesses: @score.correct_guesses,
        incorrect_guesses: @score.incorrect_guesses
      }
    )
  end

  def obtain_overwrite_choice
    @output.print Message.overwrite_prompt
    overwrite_choice = @input.gets.chomp.downcase
    until valid_yes_no?(overwrite_choice)
      @output.puts Message.yes_no_error_message
      @output.print Message.overwrite_prompt
      overwrite_choice = @input.gets.chomp.downcase
    end
    overwrite_choice
  end

  def save_exist?(path = HangmanHelper::SAVE_PATH)
    !(File.size?(path).nil? || File.size?(path).zero?)
  end

  def write_to_file(path = HangmanHelper::SAVE_PATH)
    File.write(path, game_state)
  end
end

# Represents a game of Hangman.
class Game
  include Message
  include LoadedGame
  include SavedGame

  def initialize(setup = {})
    @dict = setup.fetch(:dict, Dictionary.new(HangmanHelper::DICT_PATH))
    @word = setup.fetch(:word, Word.new(@dict.get_random_word(5, 12)))
    @score = setup.fetch(:score, Score.new)
    @input = setup.fetch(:input, $stdin)
    @output = setup.fetch(:output, $stdout)
  end

  def inspect
    "#<#{self.class}:hangman>"
  end

  def play(path = HangmanHelper::SAVE_PATH)
    until game_over?
      display_round
      turn_choice = obtain_turn_choice
      if turn_choice == 'quit'
        @output.puts Message.game_quit
        return
      elsif turn_choice == 'save'
        save_game(path)
      elsif correct_guess?(turn_choice)
        @score.add_correct_guess(turn_choice)
      else
        @score.add_incorrect_guess(turn_choice)
      end
    end
    display_round
    @output.puts human_won? ? Message.game_won : Message.game_lost(@word.word)
  end

  def run_setup(path = HangmanHelper::SAVE_PATH)
    introduce_game
    @output.puts if save_exist?(path)
    unless save_exist?(path) && %w[yes y].include?(obtain_load_choice)
      @output.puts Message.game_not_loaded
      return
    end

    load_game(path)
    @output.puts Message.game_loaded
  end

  private

  def correct_guess?(guess)
    @word.include?(guess)
  end

  def correct_guess_format?(guess)
    guess.length == 1 && /[[:alpha:]]/.match?(guess)
  end

  def display_round
    @output.puts
    @output.puts @word.form_word(@score.correct_guesses)
    @output.puts @word.form_underline
    @output.puts
    @output.puts @score.form_correct_guesses
    @output.puts @score.form_incorrect_guesses
    @output.puts @score.form_guesses_remaining
    @output.puts
  end

  def game_over?
    current_word = @word.form_word(@score.correct_guesses).split.join
    current_word == @word.word || @score.guesses_remaining.zero?
  end

  def human_won?
    @word.form_word(@score.correct_guesses).split.join == @word.word
  end

  def introduce_game
    @output.puts Message.introduction
    @output.puts
    @output.puts Message.instructions
    @output.puts
    @output.puts Message.license
  end

  def obtain_turn_choice
    @output.print Message.turn_prompt
    turn_choice = @input.gets.chomp.downcase
    until valid_turn_choice?(turn_choice)
      @output.puts Message.turn_error
      @output.print Message.turn_prompt
      turn_choice = @input.gets.chomp.downcase
    end
    turn_choice
  end

  def repeated_guess?(guess)
    @score.correct_guesses.union(@score.incorrect_guesses).include?(guess)
  end

  def valid_guess?(guess)
    correct_guess_format?(guess) && !repeated_guess?(guess)
  end

  def valid_turn_choice?(choice)
    valid_guess?(choice) || choice == 'save' || choice == 'quit'
  end

  def valid_yes_no?(response)
    %w[yes y no n].include?(response)
  end
end

# Represents the score for the game.
class Score
  attr_reader :guesses_remaining, :correct_guesses, :incorrect_guesses

  def initialize(max_incorrect_guesses = 6)
    @guesses_remaining = max_incorrect_guesses
    @correct_guesses = []
    @incorrect_guesses = []
  end

  def add_correct_guess(guess)
    check_guess_length(guess)
    @correct_guesses.append(guess)
  end

  def add_incorrect_guess(guess)
    check_guess_length(guess)
    @incorrect_guesses.append(guess)
    @guesses_remaining -= 1
  end

  def form_correct_guesses
    "Correct: #{correct_guesses.join(', ')}"
  end

  def form_incorrect_guesses
    "Incorrect: #{incorrect_guesses.join(', ')}"
  end

  def form_guesses_remaining
    "Tries left: #{@guesses_remaining}"
  end

  private

  def check_guess_length(guess)
    raise ArgumentError unless guess.length == 1
  end
end
