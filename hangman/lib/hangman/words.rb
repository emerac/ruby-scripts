# frozen_string_literal: true

# Contains classes related to words.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require 'pathname'

# Represents a file where each line contains a separate word.
class Dictionary < File
  def initialize(path)
    @path = path

    unless File.size?(@path)
      raise ArgumentError, 'file must exist and not be empty'
    end

    super(@path)

    @contents = readlines
  end

  def inspect
    "#<#{self.class}:#{Pathname.new(@path).basename}>"
  end

  def get_random_word(min, max)
    words = @contents.select { |word| word.length.between?(min, max) }
    words.empty? ? '' : words.sample
  end

  private

  def readlines
    super.map { |word| word.delete_suffix("\n") }
  end
end

# Represents the word chosen for the game.
class Word
  attr_accessor :word

  def initialize(word)
    @word = word
    @display_word = @word.chars.map { ' ' }

    @space = ' '
    @spacing_size = 2
    @spacing = @space * @spacing_size
    @underline = '─'
  end

  def form_word(visible_chars)
    reveal_letters(visible_chars)
    space_display_word
  end

  def form_underline
    @word.chars.map { @underline }.join(@spacing)
  end

  def include?(char)
    @word.include?(char)
  end

  def inspect
    "#<#{self.class}:#{@word}>"
  end

  private

  def reveal_letters(visible_chars)
    @word.chars.each_with_index do |char, index|
      @display_word[index] = char if visible_chars.include?(char)
    end
  end

  def space_display_word
    @display_word.join(@spacing)
  end
end
