# frozen_string_literal: true

# Run a set of tasks.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

task default: %w[test lint]

desc 'Test with RSpec.'
task :test do
  sh('rspec */spec \
     -r ./spec_helper.rb \
     --exclude-pattern "four-in-a-row/spec/**/*_spec.rb" \
  ')
end

desc 'Lint with RuboCop.'
task :lint do
  sh('rubocop */')
end
