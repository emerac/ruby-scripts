# frozen_string_literal: true

# Implement the merge sort algorithm.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Wraps method for running a merge sort on a list of integers.
module MergeSort
  def self.merge_sort(arr)
    return arr if arr.length < 2

    # Split into left and right halves.
    l_arr = arr[..((arr.length / 2) - 1)]
    r_arr = arr[(arr.length / 2)..]

    # Merge sort the left half.
    l_arr = merge_sort(l_arr)

    # Merge sort the right half.
    r_arr = merge_sort(r_arr)

    # Combine the left and right halves.
    s_arr = []
    until l_arr.length.zero? && r_arr.length.zero?
      if l_arr.length.zero?
        elem = r_arr.shift
      elsif r_arr.length.zero?
        elem = l_arr.shift
      else
        elem = l_arr[0] < r_arr[0] ? l_arr.shift : r_arr.shift
      end
      s_arr.push(elem)
    end
    s_arr
  end
end
