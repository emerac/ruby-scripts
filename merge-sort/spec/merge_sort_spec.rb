# frozen_string_literal: true

# Test the merge sort method.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative 'spec_helper'

describe 'MergeSort' do
  describe '#mergesort' do
    let(:unsorted_even_arr) { [8, 4, 7, 1, 2, 0, 9, 5, 6, 3] }

    let(:sorted_even_arr) { [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] }

    let(:unsorted_odd_arr) { [8, 4, 7, 1, 2, 0, 5, 6, 3] }

    let(:sorted_odd_arr) { [0, 1, 2, 3, 4, 5, 6, 7, 8] }

    context 'when the array is empty' do
      it 'returns the empty array' do
        expect(MergeSort.merge_sort([])).to eq([])
      end
    end

    context 'when the array contains one element' do
      it 'returns the array containing the one element' do
        expect(MergeSort.merge_sort([0])).to eq([0])
      end
    end

    context 'when given an array with an even number of sorted elements' do
      it 'returns an array of sorted elements' do
        expect(MergeSort.merge_sort(sorted_even_arr)).to eq(sorted_even_arr)
      end
    end

    context 'when given an array with an even number of unsorted elements' do
      it 'returns an array of sorted elements' do
        expect(MergeSort.merge_sort(unsorted_even_arr)).to eq(sorted_even_arr)
      end
    end

    context 'when given an array with an odd number of sorted elements' do
      it 'returns an array of sorted elements' do
        expect(MergeSort.merge_sort(sorted_odd_arr)).to eq(sorted_odd_arr)
      end
    end

    context 'when given an array with an odd number of unsorted elements' do
      it 'returns an array of sorted elements' do
        expect(MergeSort.merge_sort(unsorted_odd_arr)).to eq(sorted_odd_arr)
      end
    end
  end
end
