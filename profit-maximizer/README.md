# profit maximizer

Given an array representing the price of something over time, this
program will find the history points at which it should be bought and
sold in order to maximize profit.

## Usage

A script that demonstrates the profit maximizer is provided. To run this
script:

* clone the `ruby-scripts` repository
* `cd ruby-scripts/profit-maximizer`
* `bin/demo`

If the last command produces a permission error, it is likely because
the script is not executable. You can change this with the command
`chmod u+x bin/demo`.

## License

This program is licensed under the GNU General Public License. For
the full license text, view the [LICENSE](../LICENSE) file.
