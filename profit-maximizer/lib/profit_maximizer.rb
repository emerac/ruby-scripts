# frozen_string_literal: true

# Determine the best times to buy and sell based on price history.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Represents a price history profit maximizer.
class ProfitMaximizer
  def initialize(setup = {})
    @input = setup.fetch(:input, $stdin)
    @output = setup.fetch(:output, $stdout)
  end

  def maximize_profit(history)
    # Displays the best combination of buy and sell points in the
    # provided array to maximize profit.
    profit_indices = find_profit_indices(history)
    profits = find_profits(history, profit_indices)
    points = find_points(profit_indices, profits)
    @output.puts results(history, points)
  end

  private

  def results(history, points)
    # Returns a string containing the results in a readable format.
    result = history[points[1]] - history[points[0]]
    "History:\t#{history.join(', ')}\n" \
      "Buy point:\t#{points[0] + 1}\n" \
      "Sell point:\t#{points[1] + 1}\n" \
      "Net #{result.positive? ? 'gain' : 'loss'}:\t#{result}"
  end

  def find_points(profit_indices, profits)
    # Returns an array where elem one is the history array's best buy
    # index and elem two is the best sell index.
    max_profit_index = profits.index(profits.compact.max)
    [max_profit_index, profit_indices[max_profit_index]]
  end

  def find_profit_indices(history)
    # Returns an array where each index contains the best possible sell
    # history point if buying at that index.
    profit_indices = Array.new(history.length, nil)
    history.each_index do |i|
      history.each_index do |j|
        # Do not sell before buying.
        next unless j > i

        profit_indices[i] = j if profit_indices[i].nil?
        old_max_profit = history[profit_indices[i]] - history[i]
        profit_indices[i] = j if (history[j] - history[i]) > old_max_profit
      end
    end
    profit_indices
  end

  def find_profits(history, profit_indices)
    # Returns an array where each index contains the best possible
    # profit if buying at that index.
    profits = Array.new(history.length, nil)
    history.each_index do |i|
      # Buying on the last day allows no time to sell.
      next if profit_indices[i].nil?

      profits[i] = history[profit_indices[i]] - history[i]
    end
    profits
  end
end
