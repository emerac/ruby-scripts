# frozen_string_literal: true

# A game of tic tac toe.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

module TicTacToe
  # Represents a game of tic tac toe.
  class Game
    def initialize(
      board = Board.new,
      player1 = Player.new('Player 1', 'X'),
      player2 = Player.new('Player 2', 'O')
    )
      @board = board
      @player1 = player1
      @player2 = player2
      @turn_order = [@player1, @player2]
      randomize_turn_order
    end

    def game_over?
      @board.full? || @board.win?
    end

    def play
      puts introduction
      until game_over?
        puts
        @board.display_board
        puts
        turn = solicit_turn
        case turn
        when 'help'
          puts
          puts help
          puts
        when 'license'
          puts
          puts license
          puts
        when 'quit'
          puts
          puts quit_notification
          return
        else
          f_turn = turn.to_i.digits.reverse
          @board.change_value(f_turn[0], f_turn[1], @turn_order.first.symbol)
          switch_players
        end
      end

      puts
      @board.display_board
      puts
      if @board.win?
        puts win_notification
      else
        puts tie_notification
      end
    end

    def randomize_turn_order
      @turn_order.shuffle!
    end

    def solicit_turn
      # '$stdout' must be included before print statements for testing.
      # It is unknown why 'print' and 'puts' act differently.
      $stdout.print turn_prompt
      turn = @turn_order.first.enter_move
      until valid_turn?(turn)
        puts turn_error_message
        $stdout.print turn_prompt
        turn = @turn_order.first.enter_move
      end
      turn
    end

    def switch_players
      @turn_order.rotate!
    end

    def valid_turn?(response)
      return true if %w[help license quit].include?(response)

      f_response = response.to_i.digits.reverse
      # Response must be formatted '##' and space must be unoccupied.
      if f_response.length != 2 ||
         f_response.any? { |i| i < 1 || i > @board.side_length } ||
         !@board.retrieve_value(f_response[0], f_response[1]).nil?
        false
      else
        true
      end
    end

    private

    def turn_error_message
      "Error: an invalid move or choice was entered. Enter \'help\' " \
        'for more information.'
    end

    def help
      n = @board.side_length
      <<~HELP
        To enter a move:
        Enter two consecutive digits representing the row and column of
        the space on the board where you wish to place your symbol. Each
        digit must be between 1 and #{n}, inclusive. For example, to
        place a symbol in the top-left corner of the board, you would
        enter '11'.

        To display this help message again:
        Enter 'help'.

        To display this program's license:
        Enter 'license'.

        To quit the game:
        Enter 'quit'.
      HELP
    end

    def introduction
      <<~INTRODUCTION
        Welcome to tic tac toe!

        Two players take turns placing their respective symbols on the
        board. If a player can fill in an entire row, column, or
        diagonal with their symbol, they win!

        #{@turn_order.first.name} has been randomly chosen to start.
      INTRODUCTION
    end

    def license
      <<~LICENSE
        tic-tac-toe Copyright (C) 2021 emerac
        This program comes with ABSOLUTELY NO WARRANTY; This is free
        software, and you are welcome to redistribute it under certain
        conditions; You should have received a copy of the GNU General
        Public License along with this program.  If not, see
        <https://www.gnu.org/licenses/>.
      LICENSE
    end

    def quit_notification
      'The game has been quit.'
    end

    def tie_notification
      'Nobody wins! The game is a tie.'
    end

    def turn_prompt
      "#{@turn_order.first.name} (#{@turn_order.first.symbol}), please " \
        "enter a move, 'help, 'license', or 'quit': "
    end

    def win_notification
      "#{@turn_order.last.name} (#{@turn_order.last.symbol}) wins the game!"
    end
  end
end
