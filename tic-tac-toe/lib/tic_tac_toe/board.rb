# frozen_string_literal: true

# A tic tac toe board.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

module TicTacToe
  # Represents a tic tac toe board.
  class Board
    attr_reader :board, :side_length

    def initialize(side_length = 3, board = nil)
      @side_length = side_length
      @board = board.nil? ? default_board(@side_length) : board
    end

    # Row and column indexes start at one for user convenience.
    def change_value(row, column, value)
      @board[row - 1][column - 1] = value
    end

    def display_board(spacing = 3, padding = 5)
      display_length = (@board.length * spacing) + (@board.length - 1)
      @board.each_index do |i|
        puts visual_row(i, spacing).rjust(display_length + padding)
        unless i == @board.length - 1
          puts visual_divider(spacing).rjust(display_length + padding)
        end
      end
    end

    def full?
      @board.all? { |row| row.none?(nil) }
    end

    # Row and column indexes start at one for user convenience.
    def retrieve_value(row, column)
      @board[row - 1][column - 1]
    end

    def win?
      horizontal_win? || vertical_win? || forward_win? || backward_win?
    end

    private

    def backward_win?
      backward = []
      @board.length.times { |i| backward << @board[i][-1 - i] }
      backward.none?(nil) && backward.all?(backward[0])
    end

    def default_board(side_length)
      Array.new(side_length) { Array.new(side_length, nil) }
    end

    def forward_win?
      forward = []
      @board.length.times { |i| forward << @board[i][i] }
      forward.none?(nil) && forward.all?(forward[0])
    end

    def horizontal_win?
      @board.each { |row| return true if row.none?(nil) && row.all?(row[0]) }
      false
    end

    def vertical_win?
      @board.length.times do |col|
        column = []
        @board.length.times { |row| column << @board[row][col] }

        return true if column.none?(nil) && column.all?(column[0])
      end
      false
    end

    def visual_divider(spacing)
      horiz = '─'
      cross = '┼'
      (((horiz * spacing) + cross) * (@board.length - 1)) + (horiz * spacing)
    end

    def visual_row(row, spacing)
      vert = '│'
      visual_row = []
      @board[row].each do |val|
        display_val = val.nil? ? ' ' : val
        visual_row << (display_val.center(spacing) + vert)
      end
      # Remove trailing vertical bar.
      visual_row.join[..-2]
    end
  end
end
