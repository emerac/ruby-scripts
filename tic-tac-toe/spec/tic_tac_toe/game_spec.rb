# frozen_string_literal: true

# Test the Game class.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative '../spec_helper'

describe TicTacToe::Game do
  subject(:default_game) { described_class.new(default_board, player1, player2) }

  let(:default_board) { instance_double('Board', board: Array.new(3) { Array.new(3, nil) }) }
  let(:player1) { instance_double('Player 1', name: 'Player 1', symbol: 'X') }
  let(:player2) { instance_double('Player 2', name: 'Player 2', symbol: 'O') }

  describe '#intialize' do
    context 'when providing arguments' do
      it 'sets the board instance variable' do
        game_board = default_game.instance_variable_get(:@board)
        expect(game_board).to be(default_board)
      end

      it 'sets the first player instance variable' do
        game_player1 = default_game.instance_variable_get(:@player1)
        expect(game_player1).to be(player1)
      end

      it 'sets the second player instance variable' do
        game_player2 = default_game.instance_variable_get(:@player2)
        expect(game_player2).to be(player2)
      end

      it 'sets the turn order' do
        game_player1 = default_game.instance_variable_get(:@player1)
        game_player2 = default_game.instance_variable_get(:@player2)
        turn_order = default_game.instance_variable_get(:@turn_order)
        expect(turn_order).to contain_exactly(game_player1, game_player2)
      end
    end
  end

  describe '#game_over?' do
    context 'when the board is not full and there is no win' do
      it 'is not game over' do
        allow(default_board).to receive(:full?).and_return(false)
        allow(default_board).to receive(:win?).and_return(false)
        expect(default_game).not_to be_game_over
      end
    end

    context 'when the board is full' do
      it 'is game over' do
        allow(default_board).to receive(:full?).and_return(true)
        allow(default_board).to receive(:win?).and_return(false)
        expect(default_game).to be_game_over
      end
    end

    context 'when there is a win' do
      it 'is game over' do
        allow(default_board).to receive(:full?).and_return(false)
        allow(default_board).to receive(:win?).and_return(true)
        expect(default_game).to be_game_over
      end
    end
  end

  describe '#play' do
    context 'when the first move is \'help\'' do
      before do
        allow($stdout).to receive(:puts)
        allow($stdout).to receive(:print)
        allow(default_board).to receive(:win?).and_return(false)
        allow(default_board).to receive(:full?).and_return(false)
        allow(default_board).to receive(:display_board)
      end

      it 'display the help information' do
        allow(default_board).to receive(:side_length).and_return(3)
        first_player = default_game.instance_variable_get(:@turn_order).first
        allow(first_player).to receive(:enter_move).and_return('help', 'quit')
        default_game.play
        expect($stdout).to have_received(:puts).with(/help message again/)
      end

      it 're-prompts for a turn' do
        allow(default_board).to receive(:side_length).and_return(3)
        first_player = default_game.instance_variable_get(:@turn_order).first
        allow(first_player).to receive(:enter_move).and_return('help', 'quit')
        default_game.play
        expect($stdout).to have_received(:print).with(/enter a move/).twice
      end
    end

    context 'when the first move is \'license\'' do
      before do
        allow($stdout).to receive(:puts)
        allow($stdout).to receive(:print)
        allow(default_board).to receive(:win?).and_return(false)
        allow(default_board).to receive(:full?).and_return(false)
        allow(default_board).to receive(:display_board)
      end

      it 'displays the program\'s license' do
        first_player = default_game.instance_variable_get(:@turn_order).first
        allow(first_player).to receive(:enter_move).and_return('license', 'quit')
        default_game.play
        expect($stdout).to have_received(:puts).with(/GNU/)
      end

      it 're-prompts for a turn' do
        first_player = default_game.instance_variable_get(:@turn_order).first
        allow(first_player).to receive(:enter_move).and_return('license', 'quit')
        default_game.play
        expect($stdout).to have_received(:print).with(/enter a move/).twice
      end
    end

    context 'when the first move is \'quit\'' do
      before do
        allow($stdout).to receive(:puts)
        allow($stdout).to receive(:print)
        allow(default_board).to receive(:win?).and_return(false)
        allow(default_board).to receive(:full?).and_return(false)
        allow(default_board).to receive(:display_board)
      end

      it 'display a quit notification' do
        first_player = default_game.instance_variable_get(:@turn_order).first
        allow(first_player).to receive(:enter_move).and_return('quit')
        default_game.play
        expect($stdout).to have_received(:puts).with(/quit/)
      end

      it 'does not re-prompt for a turn' do
        first_player = default_game.instance_variable_get(:@turn_order).first
        allow(first_player).to receive(:enter_move).and_return('quit')
        default_game.play
        expect($stdout).to have_received(:print).with(/enter a move/).once
      end
    end

    context 'when the board is won' do
      before do
        allow($stdout).to receive(:puts)
        allow($stdout).to receive(:print)
        allow(default_board).to receive(:win?).and_return(true)
        allow(default_board).to receive(:full?).and_return(false)
        allow(default_board).to receive(:display_board)
      end

      it 'displays a win notification' do
        default_game.play
        expect($stdout).to have_received(:puts).with(/wins/)
      end
    end

    context 'when the board is full and is not won' do
      before do
        allow($stdout).to receive(:puts)
        allow($stdout).to receive(:print)
        allow(default_board).to receive(:win?).and_return(false)
        allow(default_board).to receive(:full?).and_return(true)
        allow(default_board).to receive(:display_board)
      end

      it 'displays a tie notification' do
        default_game.play
        expect($stdout).to have_received(:puts).with(/tie/)
      end
    end

    context 'when a move is entered' do
      let(:first_player) { default_game.instance_variable_get(:@turn_order).first }
      let(:second_player) { default_game.instance_variable_get(:@turn_order).last }

      before do
        allow($stdout).to receive(:puts)
        allow($stdout).to receive(:print)
        allow(default_board).to receive(:win?).and_return(false)
        allow(default_board).to receive(:full?).and_return(false)
        allow(default_board).to receive(:display_board)
        allow(default_board).to receive(:side_length).and_return(3)
        allow(default_board).to receive(:retrieve_value).and_return(nil)
        allow(default_board).to receive(:change_value)
        allow(first_player).to receive(:enter_move).and_return('13')
        allow(second_player).to receive(:enter_move).and_return('quit')
      end

      it 'sends the move to the board for placement' do
        default_game.play
        expect(default_board).to have_received(:change_value).with(1, 3, first_player.symbol)
      end

      it 'switches players after sending the move' do
        default_game.play
        expect(player2).to have_received(:enter_move).once
      end
    end
  end

  describe '#randomize_turn_order' do
    context 'when the function is called' do
      it 'uses the shuffle! method on the turn order array' do
        turn_order = default_game.instance_variable_get(:@turn_order)
        allow(turn_order).to receive(:shuffle!)
        default_game.randomize_turn_order
        expect(turn_order).to have_received(:shuffle!)
      end
    end
  end

  describe '#solicit_turn' do
    before do
      allow($stdout).to receive(:print)
      allow($stdout).to receive(:puts)
      allow(default_board).to receive(:side_length).and_return(3)
      allow(default_board).to receive(:retrieve_value).and_return(nil)
    end

    context 'when a valid choice is entered' do
      it 'outputs the turn prompt once' do
        first_player = default_game.instance_variable_get(:@turn_order).first
        allow(first_player).to receive(:enter_move).and_return('11')
        default_game.solicit_turn
        expect($stdout).to have_received(:print).with(/enter a move/).once
      end

      it 'does not output an error message' do
        first_player = default_game.instance_variable_get(:@turn_order).first
        allow(first_player).to receive(:enter_move).and_return('11')
        default_game.solicit_turn
        expect($stdout).not_to have_received(:puts).with(/Error/)
      end

      it 'returns the valid choice' do
        first_player = default_game.instance_variable_get(:@turn_order).first
        allow(first_player).to receive(:enter_move).and_return('11')
        expect(default_game.solicit_turn).to eq('11')
      end
    end

    context 'when an invalid choice is entered followed by a valid choice' do
      it 'outputs the turn prompt twice' do
        first_player = default_game.instance_variable_get(:@turn_order).first
        allow(first_player).to receive(:enter_move).and_return('00', '11')
        default_game.solicit_turn
        expect($stdout).to have_received(:print).with(/enter a move/).twice
      end

      it 'outputs an error message' do
        first_player = default_game.instance_variable_get(:@turn_order).first
        allow(first_player).to receive(:enter_move).and_return('00', '11')
        default_game.solicit_turn
        expect($stdout).to have_received(:puts).with(/Error/).once
      end

      it 'returns the valid choice' do
        first_player = default_game.instance_variable_get(:@turn_order).first
        allow(first_player).to receive(:enter_move).and_return('00', '11')
        expect(default_game.solicit_turn).to eq('11')
      end
    end
  end

  describe '#switch_players' do
    context 'when the function is called' do
      it 'uses the rotate! method on the turn order array' do
        turn_order = default_game.instance_variable_get(:@turn_order)
        allow(turn_order).to receive(:rotate!)
        default_game.switch_players
        expect(turn_order).to have_received(:rotate!)
      end
    end
  end

  describe '#valid_turn?' do
    before do
      allow(default_board).to receive(:side_length).and_return(3)
      allow(default_board).to receive(:retrieve_value).and_return(nil)
    end

    context 'when \'help\' is entered' do
      it 'is a valid turn' do
        expect(default_game.valid_turn?('help')).to be(true)
      end
    end

    context 'when \'license\' is entered' do
      it 'is a valid turn' do
        expect(default_game.valid_turn?('license')).to be(true)
      end
    end

    context 'when \'quit\' is entered' do
      it 'is a valid turn' do
        expect(default_game.valid_turn?('quit')).to be(true)
      end
    end

    context 'when \'word\' is entered' do
      it 'is not a valid turn' do
        expect(default_game.valid_turn?('word')).to be(false)
      end
    end

    context 'when a valid, unoccupied space is chosen' do
      it 'is a valid turn' do
        expect(default_game.valid_turn?('11')).to be(true)
      end
    end

    context 'when a valid, occupied space is chosen' do
      it 'is not a valid turn' do
        allow(default_board).to receive(:retrieve_value).and_return('X')
        expect(default_game.valid_turn?('11')).to be(false)
      end
    end

    context 'when the chosen space does not exist on the board' do
      it 'is not a valid turn' do
        expect(default_game.valid_turn?('44')).to be(false)
      end
    end
  end
end
