# frozen_string_literal: true

# Test the Player class.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative '../spec_helper'

describe TicTacToe::Player do
  subject(:player) { described_class.new('Player 1', 'X') }

  describe '#initialize' do
    context 'when providing a name and a symbol' do
      it 'sets the name to an instance variable' do
        expect(player).to have_attributes(name: 'Player 1')
      end

      it 'sets the symbol to an instance variable' do
        expect(player).to have_attributes(symbol: 'X')
      end
    end
  end

  describe '#enter_move' do
    before { allow($stdin).to receive(:gets).and_return('00') }

    it 'returns the string that was entered' do
      expect(player.enter_move).to eq('00')
    end
  end
end
