# frozen_string_literal: true

# Test the Board class.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative '../spec_helper'

describe TicTacToe::Board do
  describe '#initialize' do
    subject(:board_init) { described_class.new }

    matcher :be_a_default_row do
      match { |row| row.instance_of?(Array) && row.length == 3 && row.all?(nil) }
    end

    context 'when not providing any arguments' do
      it 'sets the board to be an array' do
        expect(board_init.board).to be_an(Array)
      end

      it 'sets the board to be an array of length three' do
        expect(board_init.board.length).to eq(3)
      end

      it 'sets each row in the board to a default' do
        expect(board_init.board).to all(be_a_default_row)
      end
    end
  end

  describe '#change_value' do
    subject(:board_change) { described_class.new }

    let(:old_val) { board_change.board[0][0] }
    let(:new_val) { 'O' }

    context 'when changing the value in the top-left corner' do
      it 'changes the value' do
        row = 1
        col = 1
        expect { board_change.change_value(row, col, new_val) }.to change {
          board_change.board[row - 1][col - 1]
        }.from(old_val).to(new_val)
      end
    end

    context 'when changing the value in the bottom-right corner' do
      it 'changes the value' do
        row = 3
        col = 3
        expect { board_change.change_value(row, col, new_val) }.to change {
          board_change.board[row - 1][col - 1]
        }.from(old_val).to(new_val)
      end
    end
  end

  describe '#display' do
    subject(:board_default) { described_class.new }

    before { allow($stdout).to receive(:puts) }

    it 'displays the board' do
      line_count = board_default.board.length + board_default.board.length - 1
      board_default.display_board
      expect($stdout).to have_received(:puts).exactly(line_count).times
    end
  end

  describe '#full?' do
    context 'when all spaces contain a value' do
      subject(:board_full) { described_class.new(board) }

      let(:board) { [%w[O O O], %w[O O O], %w[O O O]] }

      it 'is full' do
        expect(board_full).to be_full
      end
    end

    context 'when only one space does not contain a value' do
      subject(:board_not_full) { described_class.new(board) }

      let(:board) { [%w[O O O], ['O', nil, 'O'], %w[O O O]] }

      it 'is not full' do
        expect(board_not_full).not_to be_full
      end
    end

    context 'when no spaces contain a value' do
      subject(:board_empty) { described_class.new }

      it 'is not full' do
        expect(board_empty).not_to be_full
      end
    end
  end

  describe '#retrieve_value' do
    subject(:board_retrieve) { described_class.new(board) }

    let(:board) { [[1, 2, 3], [4, 5, 6], [7, 8, 9]] }

    context 'when retrieving the value in the top-left corner' do
      it 'returns the value' do
        row = 1
        col = 1
        expect(board_retrieve.retrieve_value(row, col)).to eq(1)
      end
    end

    context 'when retrieving the value in the bottom-right corner' do
      it 'returns the value' do
        row = 3
        col = 3
        expect(board_retrieve.retrieve_value(row, col)).to eq(9)
      end
    end
  end

  describe '#win?' do
    context 'when the board is empty' do
      subject(:board_no_win_empty) { described_class.new }

      it 'returns false' do
        expect(board_no_win_empty).not_to be_win
      end
    end

    context 'when the board is full and there is no win' do
      subject(:board_no_win_full) { described_class.new(board) }

      let(:board) { [%w[O X O], %w[X O X], %w[X O X]] }

      it 'returns false' do
        expect(board_no_win_full).not_to be_win
      end
    end

    context 'when the board contains a horizontal win' do
      subject(:board_win_horizontal) { described_class.new(board) }

      let(:board) { [[nil, nil, nil], %w[O O O], [nil, nil, nil]] }

      it 'returns true' do
        expect(board_win_horizontal).to be_win
      end
    end

    context 'when the board contains a vertical win' do
      subject(:board_win_vertical) { described_class.new(board) }

      let(:board) { [[nil, 'O', nil], [nil, 'O', nil], [nil, 'O', nil]] }

      it 'returns true' do
        expect(board_win_vertical).to be_win
      end
    end

    context 'when the board contains a forward-cross win' do
      subject(:board_win_forward_cross) { described_class.new(board) }

      let(:board) { [['O', nil, nil], [nil, 'O', nil], [nil, nil, 'O']] }

      it 'returns true' do
        expect(board_win_forward_cross).to be_win
      end
    end

    context 'when the board contains a backward-cross win' do
      subject(:board_win_backward_cross) { described_class.new(board) }

      let(:board) { [[nil, nil, 'O'], [nil, 'O', nil], ['O', nil, nil]] }

      it 'returns true' do
        expect(board_win_backward_cross).to be_win
      end
    end
  end
end
