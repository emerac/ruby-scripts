# tic-tac-toe

A game of tic tac toe on the command-line for two human players to play
against each other.

## Usage

To play the game:

* clone the `ruby-scripts` repository
* `cd ruby-scripts/tic-tac-toe`
* `bin/play`

If the last command produces a permission error, it is likely because
the script is not executable. You can change this with the command
`chmod u+x bin/play`.

## License

This program is licensed under the GNU General Public License. For
the full license text, view the [LICENSE](../LICENSE) file.
