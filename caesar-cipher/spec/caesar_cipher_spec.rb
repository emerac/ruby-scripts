# frozen_string_literal: true

# Test the CaesarCipher class.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative 'spec_helper'

describe CaesarCipher do
  let(:setup) { { output: StringIO.new } }

  let(:cipher) { described_class.new(setup) }

  describe '#encrypt' do
    context 'when string empty and shift factor positive' do
      it 'returns an empty string' do
        setup[:input] = StringIO.new("\n25")
        cipher.encrypt
        expect(setup[:output].string).to match(/.*"".*/)
      end
    end

    context 'when string empty and shift factor zero' do
      it 'returns an empty string' do
        setup[:input] = StringIO.new("\n0")
        cipher.encrypt
        expect(setup[:output].string).to match(/.*"".*/)
      end
    end

    context 'when string empty and shift factor negative' do
      it 'returns an empty string' do
        setup[:input] = StringIO.new("\n-25")
        cipher.encrypt
        expect(setup[:output].string).to match(/.*"".*/)
      end
    end

    context 'when string non-empty and shift factor positive' do
      it 'returns a forward-shifted source string' do
        setup[:input] = StringIO.new("AbcXyz123 \n25")
        cipher.encrypt
        expect(setup[:output].string).to match(/.*"ZabWxy123 ".*/)
      end
    end

    context 'when string non-empty and shift factor greater than 26' do
      it 'returns a forward-shifted source string' do
        setup[:input] = StringIO.new("AbcXyz123 \n27")
        cipher.encrypt
        expect(setup[:output].string).to match(/.*"BcdYza123 ".*/)
      end
    end

    context 'when string non-empty and shift factor zero' do
      it 'returns the source string' do
        setup[:input] = StringIO.new("AbcXyz123 \n0")
        cipher.encrypt
        expect(setup[:output].string).to match(/.*"AbcXyz123 ".*/)
      end
    end

    context 'when string non-empty and shift factor negative' do
      it 'returns a back-shifted source string' do
        setup[:input] = StringIO.new("AbcXyz123 \n-25")
        cipher.encrypt
        expect(setup[:output].string).to match(/.*"BcdYza123 ".*/)
      end
    end

    context 'when string non-empty and shift factor less than -26' do
      it 'returns a back-shifted source string' do
        setup[:input] = StringIO.new("AbcXyz123 \n-27")
        cipher.encrypt
        expect(setup[:output].string).to match(/.*"ZabWxy123 ".*/)
      end
    end

    context 'when shift factor is invalid' do
      it 'displays an error message' do
        setup[:input] = StringIO.new("x\nx\n0")
        cipher.encrypt
        expect(setup[:output].string).to match(/.*Error.*/)
      end

      it 're-prompts until the shift factor is valid' do
        setup[:input] = StringIO.new("x\nx\n0")
        cipher.encrypt
        expect(setup[:output].string).to match(/.*"x".*/)
      end
    end
  end
end
