# caesar cipher

Encrypt phrases with the
[Caesar cipher](https://en.wikipedia.org/wiki/Caesar_cipher).

## Usage

To run the program:

* clone the `ruby-scripts` repository
* `cd ruby-scripts/caesar-cipher`
* `bin/exec`

If the last command produces a permission error, it is likely because
the script is not executable. You can change this with the command
`chmod u+x bin/exec`.

## License

This program is licensed under the GNU General Public License. For
the full license text, view the [LICENSE](../LICENSE) file.
