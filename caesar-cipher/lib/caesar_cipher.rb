# frozen_string_literal: true

# Perform Caesar cipher encryption.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Represents a Caesar cipher.
class CaesarCipher
  def initialize(setup = {})
    @input = setup.fetch(:input, $stdin)
    @output = setup.fetch(:output, $stdout)
  end

  def encrypt
    @output.print string_prompt
    string = solicit_user_input
    shift_factor = solicit_shift_factor

    shifted_string = shift_string(string, shift_factor)
    @output.puts formatted_output(shifted_string)
    @output.puts
    @output.puts license
  end

  private

  def alpha_char?(char)
    !/[[:alpha:]]/.match(char).nil?
  end

  def formatted_output(shifted_string)
    "Encrypted phrase:\n\"#{shifted_string}\""
  end

  def solicit_shift_factor
    @output.print shift_factor_prompt
    response = solicit_user_input
    until valid_shift_factor?(response)
      @output.puts shift_factor_error
      @output.print shift_factor_prompt
      response = solicit_user_input
    end
    response.to_i
  end

  def solicit_user_input
    @input.gets.chomp
  end

  def license
    <<~LICENSE
      caesar-cipher Copyright (C) 2021 emerac
      This program comes with ABSOLUTELY NO WARRANTY; This is free
      software, and you are welcome to redistribute it under certain
      conditions; You should have received a copy of the GNU General
      Public License along with this program.  If not, see
      <https://www.gnu.org/licenses/>.
    LICENSE
  end

  def shift_factor_error
    'Error: the shift factor must be an integer'
  end

  def shift_factor_prompt
    "Enter a shift factor\n> "
  end

  def shift_ascii(ascii, shift_factor)
    # Modulus handles case of positive and negative shift.
    new_ascii = ascii + (shift_factor % 26)
    new_ascii = 96 + (new_ascii - 122) if new_ascii > 122
    new_ascii
  end

  def shift_string(string, shift_factor)
    new_string = string.chars
    new_string.each_with_index do |char, i|
      next unless alpha_char?(char)

      new_char = shift_ascii(char.downcase.ord, shift_factor).chr
      new_string[i] = upcase_char?(char) ? new_char.upcase : new_char
    end
    new_string.join
  end

  def string_prompt
    "Enter a phrase to encrypt\n> "
  end

  def upcase_char?(char)
    char.upcase == char
  end

  def valid_shift_factor?(shift_factor)
    !/^-?\d+$/.match(shift_factor).nil?
  end
end
