# bubble sort

Three slightly different bubble sort implementations.

## Usage

A script that demonstrates the three different bubble sorts is provided.
To run this script:

* clone the `ruby-scripts` repository
* `cd ruby-scripts/bubble-sort`
* `bin/demo`

If the last command produces a permission error, it is likely because
the script is not executable. You can change this with the command
`chmod u+x bin/demo`.

## License

This program is licensed under the GNU General Public License. For
the full license text, view the [LICENSE](../LICENSE) file.
