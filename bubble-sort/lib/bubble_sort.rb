# frozen_string_literal: true

# Various bubble sort implementations.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Represents a bubble sorter.
class BubbleSort
  # Sorts by checking after each iter if more than zero swaps were made.
  def sort_with_counter(arr)
    loop do
      swaps = 0
      arr.each_index do |i|
        next if i == arr.length - 1 || arr[i] <= arr[i + 1]

        arr[i], arr[i + 1] = arr[i + 1], arr[i]
        swaps += 1
      end
      break if swaps.zero?
    end
    arr
  end

  # Sorts by checking after each iter if a swap was made.
  def sort_with_flag(arr)
    loop do
      is_sorted = true
      arr.each_index do |i|
        next if i == arr.length - 1 || arr[i] <= arr[i + 1]

        arr[i], arr[i + 1] = arr[i + 1], arr[i]
        is_sorted = false
      end
      break if is_sorted
    end
    arr
  end

  # Sorts by knowing that an element has been sorted after each iter.
  def sort_without_check(arr)
    arr.each_index do |i|
      arr.each_index do |j|
        next if j > ((arr.length - 2) - i)

        arr[j], arr[j + 1] = arr[j + 1], arr[j] if arr[j] > arr[j + 1]
      end
    end
    arr
  end
end
