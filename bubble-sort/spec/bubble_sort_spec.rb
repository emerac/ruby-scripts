# frozen_string_literal: true

# Test the BubbleSort class.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative 'spec_helper'

describe BubbleSort do
  let(:arr) { [8, 3, 4, 6, 1, 7, 9, 2, 5] }

  let(:bubble_sort) { described_class.new }

  describe '#sort_with_counter' do
    context 'when the array is empty' do
      it 'returns an empty array' do
        expect(bubble_sort.sort_with_counter([])).to eq([])
      end
    end

    context 'when the array is populated' do
      it 'returns a sorted array' do
        expect(bubble_sort.sort_with_counter(arr)).to eq(arr.sort)
      end
    end
  end

  describe '#sort_with_flag' do
    context 'when the array is empty' do
      it 'returns an empty array' do
        expect(bubble_sort.sort_with_flag([])).to eq([])
      end
    end

    context 'when the array is populated' do
      it 'returns a sorted array' do
        expect(bubble_sort.sort_with_flag(arr)).to eq(arr.sort)
      end
    end
  end

  describe '#sort_without_check' do
    context 'when the array is empty' do
      it 'returns an empty array' do
        expect(bubble_sort.sort_without_check([])).to eq([])
      end
    end

    context 'when the array is populated' do
      it 'returns a sorted array' do
        expect(bubble_sort.sort_without_check(arr)).to eq(arr.sort)
      end
    end
  end
end
