# ruby-scripts

[![pipeline status](https://gitlab.com/emerac/ruby-scripts/badges/main/pipeline.svg)](https://gitlab.com/emerac/ruby-scripts/-/commits/main)
[![coverage report](https://gitlab.com/emerac/ruby-scripts/badges/main/coverage.svg)](https://gitlab.com/emerac/ruby-scripts/-/commits/main)

A collection of scripts, programs, and games written in Ruby. These
programs were created to practice the fundamentals of Ruby, computer
science, and software development.

## Usage

Each directory contains a different program and a README that describes
what the program does and how to use it. Clone this repository to get
them all!

## License

This collection is licensed under the GNU General Public License. For
the full license text, view the [LICENSE](LICENSE) file.
