# custom enumerables

Ruby's Enumerable mixin provides a lot of handy methods for handling
collections. For practice, I have re-implemented a number of these
methods herein.

## Usage

You can experiment with the enumerables inside an irb session. A script
that sets up a session has been provided. To use it:

* clone the `ruby-scripts` repository
* `cd ruby-scripts/custom-enumerables`
* `bin/console`

If the last command produces a permission error, it is likely because
the script is not executable. You can change this with the command
`chmod u+x bin/console`.

## License

This program is licensed under the GNU General Public License. For
the full license text, view the [LICENSE](../LICENSE) file.
