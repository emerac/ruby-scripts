# frozen_string_literal: true

# Test the Enumerable module.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative '../spec_helper'

describe Enumerable do
  describe '#my_all' do
    context 'when providing a block' do
      it 'returns true when all elements pass the block' do
        expect([1, 2, 3].my_all? { |e| e.positive? }).to be_truthy
      end

      it 'returns false when one element does not pass the block' do
        expect([1, 2, 3].my_all? { |e| e > 1 }).to be_falsy
      end

      it 'returns the same as the core method' do
        r1 = [1, 2, 3].my_all? { |e| e > 1 }
        r2 = [1, 2, 3].all? { |e| e > 1 }
        expect(r1).to eq(r2)
      end
    end

    context 'when providing an object' do
      it 'returns true when all elements are the same as the object' do
        expect([1, 1, 1].my_all?(1)).to be_truthy
      end

      it 'returns false when one element is not the same as the object' do
        expect([1, 1, 0].my_all?(1)).to be_falsy
      end

      it 'returns the same as the core method' do
        r1 = [1, 1, 0].my_all?(1)
        r2 = [1, 1, 0].all?(1)
        expect(r1).to eq(r2)
      end
    end

    context 'when providing no arguments' do
      it 'returns true when all elements are truthy' do
        expect([-1, :foo, 'string'].my_all?).to be_truthy
      end

      it 'returns false when one element is falsy' do
        expect([nil, :foo, 'string'].my_all?).to be_falsy
      end

      it 'returns true when the array is empty' do
        expect([].my_all?).to be_truthy
      end

      it 'returns the same as the core method' do
        r1 = [nil, :foo, 'string'].my_all?
        r2 = [nil, :foo, 'string'].all?
        expect(r1).to eq(r2)
      end
    end
  end

  describe '#my_any' do
    context 'when providing a block' do
      it 'returns true when one element passes the block' do
        expect([1, 2, 3].my_any? { |e| e > 2 }).to be_truthy
      end

      it 'returns false when no elements pass the block' do
        expect([1, 2, 3].my_any? { |e| e > 3 }).to be_falsy
      end

      it 'returns the same as the core method' do
        r1 = [1, 2, 3].my_any? { |e| e > 3 }
        r2 = [1, 2, 3].any? { |e| e > 3 }
        expect(r1).to eq(r2)
      end
    end

    context 'when providing an object' do
      it 'returns true when one element is the same object' do
        expect([1, 2, 3].my_any?(1)).to be_truthy
      end

      it 'returns false when no elements are the same as the object' do
        expect([1, 2, 3].my_any?(4)).to be_falsy
      end

      it 'returns the same as the core method' do
        r1 = [1, 2, 3].my_any?(1)
        r2 = [1, 2, 3].any?(1)
        expect(r1).to eq(r2)
      end
    end

    context 'when providing no arguments' do
      it 'returns true when one of the elements is truthy' do
        expect([false, nil, 0].my_any?).to be_truthy
      end

      it 'returns false when all elements are falsy' do
        expect([false, nil, nil].my_any?).to be_falsy
      end

      it 'returns true when the array is empty' do
        expect([].my_any?).to be_truthy
      end

      it 'returns the same as the core method' do
        r1 = [false, nil, nil].my_any?
        r2 = [false, nil, nil].any?
        expect(r1).to eq(r2)
      end
    end
  end

  describe '#my_count' do
    context 'when providing a block' do
      it 'returns the number of times the block returns true' do
        expect([1, 2, 3, 4, 5].my_count { |e| e > 3 }).to eq(2)
      end

      it 'returns the same as the core method' do
        r1 = [1, 2, 3, 4, 5].my_count { |e| e > 3 }
        r2 = [1, 2, 3, 4, 5].count { |e| e > 3 }
        expect(r1).to eq(r2)
      end
    end

    context 'when providing an object' do
      it 'returns a positive number when there are matching objects' do
        expect([1, 1, 1, 2, 3].my_count(1)).to eq(3)
      end

      it 'returns zero when there are no matching objects' do
        expect([1, 2, 3, 4, 5].my_count(0)).to eq(0)
      end

      it 'returns the same as the core method' do
        r1 = [1, 1, 1, 2, 3].my_count(1)
        r2 = [1, 1, 1, 2, 3].count(1)
        expect(r1).to eq(r2)
      end
    end

    context 'when providing no arguments' do
      it 'returns the number of elements when there are elements' do
        expect([1, 2, 3, 4, 5].my_count).to eq(5)
      end

      it 'returns zero when there are no elements' do
        expect([].my_count).to eq(0)
      end

      it 'returns the same as the core method' do
        r1 = [1, 2, 3, 4, 5].my_count
        r2 = [1, 2, 3, 4, 5].count
        expect(r1).to eq(r2)
      end
    end
  end

  describe '#my_each' do
    context 'when providing a block' do
      it 'performs the block on each array element' do
        arr = []
        [1, 2, 3, 4].my_each { |e| arr << e if e.even? }
        expect(arr).to eq([2, 4])
      end

      it 'can modify the array while executing the block' do
        arr = [1, 2, 3, 4]
        expect(arr.my_each { arr.clear }).to eq([])
      end

      it 'returns the same as the core method' do
        r1 = []
        r2 = []
        [1, 2, 3, 4].my_each { |e| r1 << e if e.even? }
        [1, 2, 3, 4].each { |e| r2 << e if e.even? }
        expect(r1).to eq(r2)
      end
    end
  end

  describe '#my_each_with_index' do
    context 'when providing a block' do
      it 'performs the block on each array element' do
        hash = {}
        [1, 2, 3].my_each_with_index { |e, i| hash[e] = i }
        expect(hash).to eq({ 1 => 0, 2 => 1, 3 => 2 })
      end

      it 'returns the same as the core method' do
        r1 = {}
        r2 = {}
        [1, 2, 3].my_each_with_index { |e, i| r1[e] = i }
        [1, 2, 3].each_with_index { |e, i| r2[e] = i }
        expect(r1).to eq(r2)
      end
    end
  end

  describe '#my_inject' do
    context 'when providing a block' do
      it 'sums all of the elements' do
        expect((0..9).my_inject { |acc, e| acc + e }).to eq(45)
      end
    end

    context 'when providing a symbol' do
      it 'sums all of the elements' do
        expect((0..9).my_inject(:+)).to eq(45)
      end
    end

    context 'when providing an initial and a symbol' do
      it 'returns the product of all of the elements' do
        expect((1..5).my_inject(1, :*)).to eq(120)
      end
    end

    context 'when providing an initial and a block' do
      it 'returns the product of all of the elements' do
        expect((1..5).my_inject(1) { |acc, e| acc * e }).to eq(120)
      end

      it 'returns the same as the core method' do
        r1 = (2..7).my_inject(4) { |acc, e| (acc * e) + e }
        r2 = (2..7).my_inject(4) { |acc, e| (acc * e) + e }
        expect(r1).to eq(r2)
      end
    end
  end

  describe '#my_map' do
    context 'when providing a block' do
      it 'does not raise an argument error' do
        expect { (7..10).my_map { |e| e * e } }.not_to raise_error
      end

      it 'returns a new array of elements that have passed through block' do
        expect((5..9).my_map { |e| e * e }).to eq([25, 36, 49, 64, 81])
      end

      it 'returns the same as the core method' do
        r1 = (12..31).my_map { |e| (e + 21) * e }
        r2 = (12..31).map { |e| (e + 21) * e }
        expect(r1).to eq(r2)
      end
    end

    context 'when providing a proc' do
      it 'does not raise an argument error' do
        expect { (7..10).my_map(proc { |e| e * e }) }.not_to raise_error
      end

      it 'returns a new array of elements that have passed through block' do
        expect((5..9).my_map(proc { |e| e * e })).to eq([25, 36, 49, 64, 81])
      end
    end
  end

  describe '#my_map_block' do
    context 'when providing a block' do
      it 'returns a new array of elements that have passed through block' do
        expect((7..10).my_map_block { |e| e * e }).to eq([49, 64, 81, 100])
      end

      it 'returns the same as the core method' do
        r1 = (2..22).my_map_block { |e| (e * e) + (4 * e) }
        r2 = (2..22).map { |e| (e * e) + (4 * e) }
        expect(r1).to eq(r2)
      end
    end

    context 'when providing a proc' do
      it 'raises an argument error' do
        my_proc = proc { |e| e * e }
        expect { (7..10).my_map_block(my_proc) }.to raise_error(ArgumentError)
      end
    end
  end

  describe '#my_map_proc' do
    context 'when providing a proc' do
      it 'returns a new array of elements that have passed through proc' do
        expect((3..6).my_map_proc(proc { |e| e * e })).to eq([9, 16, 25, 36])
      end
    end

    context 'when providing a block' do
      it 'raises an argument error' do
        expect { (7..10).my_map_proc { |e| e } }.to raise_error(ArgumentError)
      end
    end
  end

  describe '#my_none?' do
    context 'when providing a block' do
      it 'returns true when none of the elements meet the criteria' do
        expect(%w[hello world].my_none? { |e| e.length == 4 }).to be_truthy
      end

      it 'returns false when one of the elements meets the criteria' do
        expect(%w[hello world].my_none? { |e| e.start_with?('h') }).to be_falsy
      end
    end

    context 'when providing a object' do
      it 'returns true when none of the elements match the object' do
        expect([1, 2, 3].my_none?(Symbol)).to be_truthy
      end

      it 'returns false when one of the elements matches the object' do
        expect([1, 2, :+].my_none?(:+)).to be_falsy
      end
    end
  end

  describe '#my_select' do
    context 'when providing a block' do
      it 'returns a new array of all elements that match the criteria' do
        expect((4..12).my_select { |e| e.odd? }).to eq([5, 7, 9, 11])
      end

      it 'returns the same as the core method' do
        r1 = (27..51).my_select { |e| (e % 5).zero? }
        r2 = (27..51).select { |e| (e % 5).zero? }
        expect(r1).to eq(r2)
      end
    end
  end

  describe '#my_select2' do
    context 'when providing a block' do
      it 'returns a new array of all elements that match the criteria' do
        expect((4..12).my_select2 { |e| e.odd? }).to eq([5, 7, 9, 11])
      end

      it 'returns the same as the core method' do
        r1 = (27..51).my_select2 { |e| (e % 5).zero? }
        r2 = (27..51).select { |e| (e % 5).zero? }
        expect(r1).to eq(r2)
      end
    end
  end
end

describe Helper do
  describe '#multiply_elems' do
    it 'returns zero when the array is empty' do
      expect(described_class.multiply_elems([])).to eq(0)
    end

    it 'returns the correct amount when given an array of integers' do
      expect(described_class.multiply_elems([2, 4, 5])).to eq(40)
    end
  end
end
