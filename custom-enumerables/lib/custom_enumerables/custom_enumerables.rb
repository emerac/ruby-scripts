# frozen_string_literal: true

# Extend the Enumerable module.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Re-implementations of Enumerable module methods.
# See Ruby docs for more information on how to use these methods.
module Enumerable
  # Accepts a block, an argument, or no argument at all.
  def my_all?(obj = nil)
    arr = []
    if block_given?
      my_each { |e| arr << yield(e) }
    elsif !obj.nil?
      my_each { |e| arr << (e === obj) }
    else
      my_each { |e| arr << !(e.nil? || e == false) }
    end
    arr.my_each { |e| return false if e == false }
    true
  end

  # Accepts a block, an argument, or no argument at all.
  def my_any?(obj = nil)
    return true if length.zero?

    arr = []
    if block_given?
      my_each { |e| arr << yield(e) }
    elsif !obj.nil?
      my_each { |e| arr << (e === obj) }
    else
      my_each { |e| arr << !(e.nil? || e == false) }
    end
    arr.my_each { |e| return true if e == true }
    false
  end

  # Accepts a block, an argument, or no argument at all.
  def my_count(obj = nil)
    count = 0
    if block_given?
      my_each { |e| count += 1 if yield(e) == true }
    elsif !obj.nil?
      my_each { |e| count += 1 if e.eql?(obj) }
    else
      my_each { count += 1 }
    end
    count
  end

  # Accepts a block.
  def my_each
    for elem in self
      yield(elem)
    end
  end

  # Accepts a block.
  def my_each_with_index
    index = 0
    for elem in self
      yield(elem, index)
      index += 1
    end
  end

  # Accepts block, symbol, or initial with either block or symbol.
  def my_inject(arg1 = nil, arg2 = nil)
    acc = 0
    if arg1.instance_of?(Integer) && block_given?
      acc = arg1
      my_each { |e| acc = yield(acc, e) }
    elsif arg1.instance_of?(Integer) && arg2.instance_of?(Symbol)
      acc = arg1
      my_each { |e| acc = arg2.to_proc.call(acc, e) }
    elsif arg1.instance_of?(Symbol) && arg2.nil?
      my_each { |e| acc = arg1.to_proc.call(acc, e) }
    elsif arg1.nil? && arg2.nil? && block_given?
      my_each { |e| acc = yield(acc, e) }
    end
    acc
  end

  # Accepts a proc or a block. If both, only the proc is executed.
  def my_map(user_proc = nil)
    arr = []
    if block_given?
      my_each { |e| arr << yield(e) }
    else
      my_each { |e| arr << user_proc.call(e) }
    end
    arr
  end

  # Accepts a block.
  def my_map_block
    arr = []
    my_each { |e| arr << yield(e) }
    arr
  end

  # Accepts a proc.
  def my_map_proc(user_proc)
    arr = []
    my_each { |e| arr << user_proc.call(e) }
    arr
  end

  # Accepts a block or an object.
  def my_none?(obj = nil)
    arr = []
    flag = true
    if block_given?
      my_each { |e| arr << yield(e) }
    else
      my_each { |e| arr << (e === obj) }
    end
    arr.my_each { |e| return false if e == true }
    flag
  end

  # Accepts a block. Does not re-use #my_each.
  def my_select
    arr = []
    for elem in self
      arr << elem if yield(elem)
    end
    arr
  end

  # Accepts a block. Re-uses #my_each.
  def my_select2
    arr = []
    my_each { |e| arr << e if yield(e) == true }
    arr
  end
end

# Convenience methods for custom enumerables.
module Helper
  def self.multiply_elems(arr, initial = 1)
    return 0 if arr.length.zero?

    arr.my_inject(initial, :*)
  end
end
