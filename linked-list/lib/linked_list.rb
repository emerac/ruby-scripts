# frozen_string_literal: true

# Implement a linked list and associated helper methods.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Wraps a linked list implementation.
module LinkedList
  # Represents a list of nodes.
  class LinkedList
    attr_accessor :head

    def initialize
      @head = nil
    end

    def at(index)
      raise IndexError if index.negative? || index >= size

      each_with_index { |node, i| return node if i == index }
    end

    def append(value)
      if @head.nil?
        @head = Node.new(value)
        return
      end

      each do |node|
        if node.next_node.nil?
          node.next_node = Node.new(value)
          return
        end
      end
    end

    def contains?(value)
      each { |node| return true if node.value == value }
      false
    end

    def find(value)
      each_with_index { |node, i| return i if node.value == value }
    end

    def head_node
      @head
    end

    def insert_at(index, value)
      last_index = size.zero? ? 0 : size - 1
      raise IndexError if index.negative? || index > last_index

      if index.zero?
        prepend(value)
      elsif index == size - 1
        append(value)
      else
        insert_in_middle(index, value)
      end
    end

    def pop
      return nil if @head.nil?

      each_with_previous do |current_node, previous_node|
        if current_node.next_node.nil?
          previous_node.next_node = nil
          return current_node
        end
      end
    end

    def prepend(value)
      @head = Node.new(value, @head)
    end

    def remove_at(index)
      raise IndexError if size.zero? || index.negative? || index > size - 1

      if index.zero?
        @head = @head.next_node
      elsif index == size - 1
        pop
      else
        remove_from_middle(index)
      end
    end

    def size
      count = 0
      return count if @head.nil?

      each { count += 1 }
      count
    end

    def tail_node
      return nil if @head.nil?

      each { |node| return node if node.next_node.nil? }
    end

    def to_s
      return 'nil' if @head.nil?

      values = []
      each { |node| values.append(node.value) }
      values.map { |node_value| "( #{node_value} )" }.join(' -> ') << ' -> nil'
    end

    private

    def each
      current_node = @head
      until current_node.nil?
        yield(current_node)
        current_node = current_node.next_node
      end
    end

    def each_with_index
      current_index = 0
      each do |node|
        yield(node, current_index)
        current_index += 1
      end
    end

    def each_with_previous
      previous_node = nil
      each do |node|
        yield(node, previous_node)
        previous_node = node
      end
    end

    def insert_in_middle(index, value)
      current_index = 0
      each_with_previous do |current_node, previous_node|
        if current_index == index
          previous_node.next_node = Node.new(value, current_node)
        end
        current_index += 1
      end
    end

    def remove_from_middle(index)
      current_index = 0
      each_with_previous do |current_node, previous_node|
        if current_index == index
          previous_node.next_node = current_node.next_node
        end
        current_index += 1
      end
    end
  end

  # Represents a single node in a linked list.
  class Node
    attr_accessor :value, :next_node

    def initialize(value = nil, next_node = nil)
      @value = value
      @next_node = next_node
    end
  end
end
