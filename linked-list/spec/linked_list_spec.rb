# frozen_string_literal: true

# Test the LinkedList module.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative 'spec_helper'

describe 'LinkedList::Node' do
  describe '#initialize' do
    context 'when providing no arguments' do
      it 'initializes the object with a default value of nil' do
        expect(LinkedList::Node.new.value).to be_nil
      end

      it 'initializes the object with a default next node of nil' do
        expect(LinkedList::Node.new.next_node).to be_nil
      end
    end

    context 'when providing arguments' do
      it 'initializes the object with the provided value' do
        expect(LinkedList::Node.new(0).value).not_to be_nil
      end

      it 'initializes the object with the provided next node' do
        next_node = LinkedList::Node.new
        expect(LinkedList::Node.new(nil, next_node).next_node).not_to be_nil
      end
    end
  end
end

describe 'LinkedList::LinkedList' do
  let(:values) { [2, 4, 7, 6, 1, 3, 9, 5, 8] }

  let(:empty_linked_list) { LinkedList::LinkedList.new }

  let(:pop_linked_list) do
    linked_list = LinkedList::LinkedList.new
    values.each_index { |index| linked_list.append(values[index]) }
    linked_list
  end

  describe '#initialize' do
    context 'when providing no arguments' do
      it 'creates a list with no nodes' do
        expect(LinkedList::LinkedList.new.head).to be_nil
      end
    end
  end

  describe '#at' do
    context 'when the list is empty' do
      it 'raises an index error when trying to retrieve the first node' do
        expect { empty_linked_list.at(0) }.to raise_error(IndexError)
      end

      it 'raises an index error when trying to retrieve the later nodes' do
        expect { empty_linked_list.at(9) }.to raise_error(IndexError)
      end
    end

    context 'when the list is not empty' do
      it 'retrieves the first node in the list when given the first index' do
        expect(pop_linked_list.at(0).value).to eq(2)
      end

      it 'retrieves the last node in the list when given the last index' do
        index = find_size(pop_linked_list) - 1
        expect(pop_linked_list.at(index).value).to eq(8)
      end

      it 'raises an index error when given a nonexistent index' do
        index = find_size(pop_linked_list)
        expect { empty_linked_list.at(index) }.to raise_error(IndexError)
      end
    end
  end

  describe '#append' do
    # Avoid using #append to create an object for testing #append.
    let(:pop_linked_list) do
      linked_list = LinkedList::LinkedList.new
      linked_list.head = LinkedList::Node.new(0)
      linked_list.head.next_node = LinkedList::Node.new(1)
      linked_list
    end

    context 'when the list is empty' do
      it 'adds a node with the provided value' do
        empty_linked_list.append(0)
        expect(empty_linked_list.head.value).to eq(0)
      end
    end

    context 'when the list is not empty' do
      it 'adds a new node to the end of the list' do
        pop_linked_list.append(2)
        expect(pop_linked_list.head.next_node.next_node.value).to eq(2)
      end
    end
  end

  describe '#contains?' do
    context 'when the list is empty' do
      it 'returns false' do
        expect(empty_linked_list.contains?(0)).to be_falsy
      end
    end

    context 'when the list contains multiple elements' do
      it 'returns false if the value does not exist in the list' do
        expect(pop_linked_list.contains?(0)).to be_falsy
      end

      it 'returns true if the value exists in the list' do
        expect(pop_linked_list.contains?(1)).to be_truthy
      end
    end
  end

  describe '#find' do
    context 'when the list is empty' do
      it 'returns nil' do
        expect(empty_linked_list.find(0)).to be_nil
      end
    end

    context 'when the list contains multiple elements' do
      it 'returns nil if the value does not exist in the list' do
        expect(pop_linked_list.find(0)).to be_nil
      end

      it 'returns the index of the node if the value exists in the list' do
        expect(pop_linked_list.find(1)).to eq(4)
      end

      it 'returns the index of the first node in the list that matches' do
        values.append(1)
        expect(pop_linked_list.find(1)).to eq(4)
      end
    end
  end

  describe '#head_node' do
    context 'when the list is empty' do
      it 'returns nil' do
        expect(empty_linked_list.head_node).to be_nil
      end
    end

    context 'when the list contains multiple elements' do
      it 'returns the first node in the list' do
        expect(pop_linked_list.head_node).to eq(pop_linked_list.head)
      end
    end
  end

  describe '#insert_at' do
    context 'when the list is empty' do
      it 'raises an index error if the index is less than zero' do
        expect do
          empty_linked_list.insert_at(-1, 0)
        end.to raise_error(IndexError)
      end

      it 'raises an index error if the index is greater than zero' do
        expect do
          empty_linked_list.insert_at(1, 0)
        end.to raise_error(IndexError)
      end

      it 'inserts a node with the given value into the list' do
        empty_linked_list.insert_at(0, 0)
        expect(find_previous_node(empty_linked_list, nil).value).to eq(0)
      end
    end

    context 'when the list contains multiple elements' do
      it 'raises an index error if the index is less than zero' do
        expect do
          pop_linked_list.insert_at(-1, 0)
        end.to raise_error(IndexError)
      end

      it 'raises an index error if the index is beyond the last index' do
        expect do
          pop_linked_list.insert_at(find_size(pop_linked_list), 0)
        end.to raise_error(IndexError)
      end

      it 'inserts a node with the given value at the front of the list' do
        pop_linked_list.insert_at(0, 0)
        expect(find_node(pop_linked_list, 0).value).to eq(0)
      end

      it 'preserves the other nodes when inserting at the front' do
        original_size = find_size(pop_linked_list)
        pop_linked_list.insert_at(0, 0)
        expect(find_size(pop_linked_list)).to eq(original_size + 1)
      end

      it 'inserts a node with the given value at the end of the list' do
        pop_linked_list.insert_at(find_size(pop_linked_list) - 1, 0)
        expect(find_node(pop_linked_list, 0).value).to eq(0)
      end

      it 'preserves the other nodes when inserting at the end' do
        original_size = find_size(pop_linked_list)
        pop_linked_list.insert_at(find_size(pop_linked_list) - 1, 0)
        expect(find_size(pop_linked_list)).to eq(original_size + 1)
      end

      it 'inserts a node with the given value in the middle of the list' do
        pop_linked_list.insert_at(find_size(pop_linked_list) / 2, 0)
        expect(find_node(pop_linked_list, 0).value).to eq(0)
      end

      it 'preserves the other nodes when inserting into the middle' do
        original_size = find_size(pop_linked_list)
        pop_linked_list.insert_at(find_size(pop_linked_list) / 2, 0)
        expect(find_size(pop_linked_list)).to eq(original_size + 1)
      end
    end
  end

  describe '#pop' do
    context 'when the list is empty' do
      it 'returns nil' do
        expect(empty_linked_list.pop).to be_nil
      end
    end

    context 'when the list is not empty' do
      it 'returns the last node in the list' do
        last_node = find_previous_node(pop_linked_list, nil)
        expect(pop_linked_list.pop).to eq(last_node)
      end

      it 'makes what was the penultimate node the new last node' do
        penultimate_node = find_node(pop_linked_list, 5)
        pop_linked_list.pop
        last_node = find_previous_node(pop_linked_list, nil)
        expect(last_node).to eq(penultimate_node)
      end
    end
  end

  describe '#prepend' do
    context 'when the list is empty' do
      it 'adds a node with the provided value' do
        empty_linked_list.prepend(0)
        expect(empty_linked_list.head.value).to eq(0)
      end
    end

    context 'when the list is not empty' do
      it 'adds a new node to the beginning of the list' do
        pop_linked_list.prepend(0)
        expect(pop_linked_list.head.value).to eq(0)
      end

      it 'preserves subsequent list nodes' do
        old_head_value = pop_linked_list.head.value
        pop_linked_list.prepend(0)
        expect(pop_linked_list.head.next_node.value).to eq(old_head_value)
      end
    end
  end

  describe '#remove_at' do
    context 'when the list is empty' do
      it 'raises an index error' do
        expect do
          empty_linked_list.remove_at(0)
        end.to raise_error(IndexError)
      end
    end

    context 'when the list contains multiple elements' do
      it 'raises an index error if the index is less than zero' do
        expect do
          pop_linked_list.remove_at(-1)
        end.to raise_error(IndexError)
      end

      it 'raises an index error if the index is beyond the last index' do
        expect do
          pop_linked_list.remove_at(find_size(pop_linked_list))
        end.to raise_error(IndexError)
      end

      it 'removes the node from the front of the list' do
        pop_linked_list.remove_at(0)
        expect(pop_linked_list.head.value).to eq(4)
      end

      it 'preserves the other nodes when removing at the front' do
        original_size = find_size(pop_linked_list)
        pop_linked_list.remove_at(0)
        expect(find_size(pop_linked_list)).to eq(original_size - 1)
      end

      it 'removes the node from the end of the list' do
        pop_linked_list.remove_at(find_size(pop_linked_list) - 1)
        last_node = find_previous_node(pop_linked_list, nil)
        expect(last_node.value).to eq(5)
      end

      it 'preserves the other nodes when removing from the end' do
        original_size = find_size(pop_linked_list)
        pop_linked_list.remove_at(find_size(pop_linked_list) - 1)
        expect(find_size(pop_linked_list)).to eq(original_size - 1)
      end

      it 'removes a node from the middle of the list' do
        pop_linked_list.remove_at(find_size(pop_linked_list) / 2)
        expect(find_node(pop_linked_list, 6).next_node.value).to eq(3)
      end

      it 'preserves the other nodes when removing from the middle' do
        original_size = find_size(pop_linked_list)
        pop_linked_list.remove_at(find_size(pop_linked_list) / 2)
        expect(find_size(pop_linked_list)).to eq(original_size - 1)
      end
    end
  end

  describe '#size' do
    context 'when the list is empty' do
      it 'returns zero' do
        expect(empty_linked_list.size).to eq(0)
      end
    end

    context 'when the list contains multiple elements' do
      it 'returns the number of elements in the list' do
        expect(pop_linked_list.size).to eq(values.length)
      end
    end
  end

  describe '#tail_node' do
    context 'when the list is empty' do
      it 'returns nil' do
        expect(empty_linked_list.tail_node).to be_nil
      end
    end

    context 'when the list contains multiple elements' do
      it 'returns the last node in the list' do
        last_node = find_previous_node(pop_linked_list, nil)
        expect(pop_linked_list.tail_node).to eq(last_node)
      end
    end
  end

  describe '#to_s' do
    context 'when the list is empty' do
      it 'returns a depiction of an empty list' do
        expect(empty_linked_list.to_s).to eq('nil')
      end
    end

    context 'when the list contains multiple elements' do
      it 'returns a depiction of a populated list' do
        str = values.map { |value| "( #{value} )" }.join(' -> ') << ' -> nil'
        expect(pop_linked_list.to_s).to eq(str)
      end
    end
  end
end

def find_node(list, value)
  node = list.head
  if value.nil?
    node = node.next_node until node.nil?
  else
    node = node.next_node until node.value == value
  end
  node
end

def find_previous_node(list, value)
  node = list.head
  if value.nil?
    node = node.next_node until node.next_node.nil?
  else
    node = node.next_node until node.next_node.value == value
  end
  node
end

def find_size(list)
  count = 0
  node = list.head
  until node.nil?
    node = node.next_node
    count += 1
  end
  count
end
