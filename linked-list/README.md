# linked list

This project implements a linked list. This implementation allows:

* a linked list to be created
* nodes to be prepended, appended, and popped
* the list size to be returned
* head and tail nodes to be returned
* nodes at given indexes to be returned
* nodes to be inserted at and removed from given indexes
* the list to be checked for a value
* the index of a value to be returned
* a diagram of the list to be displayed

## Usage

You can experiment with the linked list inside an irb session. A script
that sets up a session has been provided. To use it:

* clone the `ruby-scripts` repository
* `cd ruby-scripts/linked-list`
* `bin/console`

If the last command produces a permission error, it is likely because
the script is not executable. You can change this with the command
`chmod u+x bin/console`.

## License

This program is licensed under the GNU General Public License. For
the full license text, view the [LICENSE](../LICENSE) file.
