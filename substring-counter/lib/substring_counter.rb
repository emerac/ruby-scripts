# frozen_string_literal: true

# Count the occurrences of provided substrings appearing in a string.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Represents a counter of substrings occurring in a string.
class SubstringCounter
  # Returns a hash containing the occurring dict words and their counts.
  def substrings(string, dict)
    subs = {}
    # At each char in string, look ahead to see if a dict word occurs.
    string.downcase.chars.each_index do |i|
      dict.each do |word|
        next unless string.downcase[i..(i + word.length - 1)] == word

        subs.key?(word.to_sym) ? subs[word.to_sym] += 1 : subs[word.to_sym] = 1
      end
    end
    subs
  end
end
