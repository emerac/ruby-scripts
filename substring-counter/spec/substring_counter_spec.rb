# frozen_string_literal: true

# Test the SubstringCounter class.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative 'spec_helper'

describe SubstringCounter do
  let(:dict) do
    %w[below down go going horn how howdy it i low own part partner sit]
  end
  let(:counter) { described_class.new }

  describe '#substrings' do
    context 'when the string is empty' do
      let(:string) { '' }

      it 'returns an empty hash' do
        expect(counter.substrings(string, dict)).to eq({})
      end
    end

    context 'when the string is a single word' do
      let(:string) { 'below' }

      it 'returns a hash of dict words appearing in the string' do
        expect(counter.substrings(string, dict).length).to eq(2)
      end

      it 'returns a hash describing how many times each word appears' do
        result = { below: 1, low: 1 }
        expect(counter.substrings(string, dict)).to eq(result)
      end
    end

    context 'when the string contains multiple words' do
      let(:string) { 'Howdy partner, sit down! How\'s it going?' }

      it 'returns a hash of dict words appearing in the string' do
        expect(counter.substrings(string, dict).length).to eq(11)
      end

      it 'returns a hash describing how many times each word appears' do
        result = {
          down: 1, go: 1, going: 1, how: 2, howdy: 1, it: 2, i: 3,
          own: 1, part: 1, partner: 1, sit: 1
        }
        expect(counter.substrings(string, dict)).to eq(result)
      end
    end
  end
end
