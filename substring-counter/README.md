# substring counter

A program that can be used to count the number of times various
substrings occur in a given text.

## Usage

A script that demonstrates the substring counter is provided. To run
this script:

* clone the `ruby-scripts` repository
* `cd ruby-scripts/substring-counter`
* `bin/demo`

If the last command produces a permission error, it is likely because
the script is not executable. You can change this with the command
`chmod u+x bin/demo`.

## License

This program is licensed under the GNU General Public License. For
the full license text, view the [LICENSE](../LICENSE) file.
