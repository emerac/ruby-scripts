# frozen_string_literal: true

# Test the Tree class.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative '../spec_helper'

describe Tree::Tree do
  let(:tree1) { described_class.new([1, 2, 3, 4, 5, 6, 7, 8, 9]) }

  let(:tree2) do
    arr = [1, 7, 4, 23, 8, 9, 4, 3, 5, 7, 9, 67, 6345, 324]
    described_class.new(arr)
  end

  describe '#initialize' do
    it 'raises an error if the provided argument is not an array' do
      expect { described_class.new(0) }.to raise_error(ArgumentError)
    end

    it 'raises an error if the provided array is empty' do
      expect { described_class.new([]) }.to raise_error(ArgumentError)
    end

    it 'raises an error if the provided array contains non-integers' do
      expect { described_class.new([0, nil, 1]) }.to raise_error(ArgumentError)
    end

    context 'when provided a simple, valid array' do
      let(:tree) { described_class.new([0]) }

      it 'does not a raise an error' do
        expect { tree }.not_to raise_error
      end

      it 'stores the root node of the tree in an instance variable' do
        expect(tree.root.class).to eq(Tree::Node)
      end
    end

    context 'when provided a moderately-complex, valid array' do
      it 'does not a raise an error' do
        expect { tree1 }.not_to raise_error
      end

      it 'stores the root node of the tree in an instance variable' do
        expect(tree1.root.class).to eq(Tree::Node)
      end

      it 'correctly sets the leaf node that is first from the left' do
        expect(tree1.root.left.left.data).to eq(1)
      end

      it 'correctly sets the leaf node that is second from the left' do
        expect(tree1.root.left.right.right.data).to eq(4)
      end

      it 'correctly sets the leaf node that is third from the left' do
        expect(tree1.root.right.left.data).to eq(6)
      end

      it 'correctly sets the leaf node that is fourth from the left' do
        expect(tree1.root.right.right.right.data).to eq(9)
      end
    end

    context 'when provided a complex, valid array' do
      it 'does not a raise an error' do
        expect { tree2 }.not_to raise_error
      end

      it 'stores the root node of the tree in an instance variable' do
        expect(tree2.root.class).to eq(Tree::Node)
      end

      it 'correctly sets the leaf node that is first from the left' do
        expect(tree2.root.left.left.right.data).to eq(3)
      end

      it 'correctly sets the leaf node that is second from the left' do
        expect(tree2.root.left.right.right.data).to eq(7)
      end

      it 'correctly sets the leaf node that is third from the left' do
        expect(tree2.root.right.left.right.data).to eq(23)
      end

      it 'correctly sets the leaf node that is fourth from the left' do
        expect(tree2.root.right.right.right.data).to eq(6345)
      end
    end
  end

  describe '#balanced' do
    context 'when the tree contains only a root node' do
      it 'returns true' do
        expect(described_class.new([0]).balanced?).to be_truthy
      end
    end

    context 'when the tree has multiple nodes and is balanced' do
      it 'returns true' do
        expect(tree1.balanced?).to be_truthy
      end
    end

    context 'when the tree has multiple nodes and is not balanced' do
      it 'returns false' do
        tree1.root.right.right.right.right = Tree::Node.new(10)
        expect(tree1.balanced?).to be_falsy
      end
    end
  end

  describe '#delete' do
    context 'when the provided argument is not an integer' do
      it 'raises an argument error' do
        expect { tree1.delete(nil) }.to raise_error(ArgumentError)
      end
    end

    context 'when the value does not exist in the tree' do
      it 'does not change the tree' do
        original_level_order = level_order(tree1)
        tree1.delete(0)
        expect(level_order(tree1)).to eq(original_level_order)
      end
    end

    context 'when the value is a node that has no children' do
      it 'deletes the node' do
        tree1.delete(9)
        expect(level_order(tree1)).not_to include(9)
      end
    end

    context 'when the value is a node that has one child on the right' do
      it 'deletes the node' do
        tree1.delete(3)
        expect(level_order(tree1)).not_to include(3)
      end
    end

    context 'when the value is a node that has one child on the left' do
      it 'deletes the node' do
        tree1.root.left.left.left = Tree::Node.new(0)
        tree1.delete(1)
        expect(level_order(tree1)).not_to include(1)
      end
    end

    context 'when the value is a node that has two children' do
      it 'deletes the node' do
        tree1.delete(2)
        expect(level_order(tree1)).not_to include(2)
      end
    end
  end

  describe '#depth' do
    context 'when the value is not an integer' do
      it 'raises an error' do
        expect { tree1.depth(nil) }.to raise_error(ArgumentError)
      end
    end

    context 'when the value does not exist in the tree' do
      it 'returns nil' do
        expect(tree1.depth(0)).to be_nil
      end
    end

    context 'when the value refers to the root node' do
      it 'returns the depth of the root node' do
        expect(tree1.depth(5)).to eq(0)
      end
    end

    context 'when the value refers to a sub-tree node' do
      it 'returns the depth of the sub-tree node' do
        expect(tree1.depth(3)).to eq(2)
      end
    end
  end

  describe '#display' do
    it 'outputs a diagram of the moderately-complex tree' do
      expect { tree1.display }.to output(/(-*\d+\n){9}/m).to_stdout
    end

    it 'outputs a diagram of the complex tree' do
      expect { tree2.display }.to output(/(-*\d+\n){11}/m).to_stdout
    end
  end

  describe '#find' do
    context 'when the value does not exist in the tree' do
      it 'returns nil' do
        expect(tree1.find(0)).to be_nil
      end
    end

    context 'when the value does exist in the tree' do
      it 'returns the node' do
        expect(tree1.find(1).data).to eq(1)
      end
    end
  end

  describe '#height' do
    it 'returns the height of the tree for a simple tree' do
      expect(tree1.height).to eq(3)
    end

    it 'returns the height of the tree for a moderately-complex tree' do
      tree2.root.right.right.right.left = Tree::Node.new(500)
      expect(tree2.height).to eq(4)
    end
  end

  describe '#inorder' do
    context 'when not provided a block' do
      it 'returns an array of values' do
        expect(tree1.inorder).to eq([1, 2, 3, 4, 5, 6, 7, 8, 9])
      end
    end

    context 'when provided a block' do
      it 'yields each node to the block' do
        expect { |b| tree1.inorder(&b) }.to yield_control.exactly(9).times
      end

      it 'returns nil' do
        expect(tree1.inorder { |node| node.data }).to be_nil
      end
    end
  end

  describe '#insert_iterative' do
    context 'when the provided argument is not an integer' do
      it 'raises an argument error' do
        expect { tree1.insert_iterative(nil) }.to raise_error(ArgumentError)
      end
    end

    context 'when the value exists in the tree' do
      it 'does not change the tree' do
        original_level_order = level_order(tree1)
        tree1.insert_iterative(1)
        expect(level_order(tree1)).to eq(original_level_order)
      end
    end

    context 'when the value does not exist in the tree' do
      it 'inserts the value as a right leaf node in the correct location' do
        tree2.insert_iterative(25)
        expect(tree2.root.right.left.right.right.data).to eq(25)
      end

      it 'inserts the value as a left leaf node in the correct location' do
        tree2.insert_iterative(300)
        expect(tree2.root.right.right.left.data).to eq(300)
      end
    end
  end

  describe '#insert_recursive' do
    context 'when the provided argument is not an integer' do
      it 'raises an argument error' do
        expect { tree1.insert_recursive(nil) }.to raise_error(ArgumentError)
      end
    end

    context 'when the value exists in the tree' do
      it 'does not change the tree' do
        original_level_order = level_order(tree1)
        tree1.insert_recursive(1)
        expect(level_order(tree1)).to eq(original_level_order)
      end
    end

    context 'when the value does not exist in the tree' do
      it 'inserts the value as a right leaf node in the correct location' do
        tree2.insert_recursive(25)
        expect(tree2.root.right.left.right.right.data).to eq(25)
      end

      it 'inserts the value as a left leaf node in the correct location' do
        tree2.insert_recursive(300)
        expect(tree2.root.right.right.left.data).to eq(300)
      end
    end
  end

  describe '#level_order_iterative' do
    context 'when not provided a block' do
      it 'returns an array of values' do
        expect(tree1.level_order_iterative).to eq([5, 2, 7, 1, 3, 6, 8, 4, 9])
      end
    end

    context 'when provided a block' do
      it 'yields each node to the block' do
        expect do |b|
          tree1.level_order_iterative(&b)
        end.to yield_control.exactly(9).times
      end

      it 'returns nil' do
        expect(tree1.level_order_iterative { |node| node.data }).to be_nil
      end
    end
  end

  describe '#level_order_recursive' do
    context 'when not provided a block' do
      it 'returns an array of values' do
        expect(tree1.level_order_recursive).to eq([5, 2, 7, 1, 3, 6, 8, 4, 9])
      end
    end

    context 'when provided a block' do
      it 'yields each node to the block' do
        expect do |b|
          tree1.level_order_recursive(&b)
        end.to yield_control.exactly(9).times
      end

      it 'returns nil' do
        expect(tree1.level_order_recursive { |node| node.data }).to be_nil
      end
    end
  end

  describe '#postorder' do
    context 'when not provided a block' do
      it 'returns an array of values' do
        expect(tree1.postorder).to eq([1, 4, 3, 2, 6, 9, 8, 7, 5])
      end
    end

    context 'when provided a block' do
      it 'yields each node to the block' do
        expect { |b| tree1.postorder(&b) }.to yield_control.exactly(9).times
      end

      it 'returns nil' do
        expect(tree1.postorder { |node| node.data }).to be_nil
      end
    end
  end

  describe '#preorder' do
    context 'when not provided a block' do
      it 'returns an array of values' do
        expect(tree1.preorder).to eq([5, 2, 1, 3, 4, 7, 6, 8, 9])
      end
    end

    context 'when provided a block' do
      it 'yields each node to the block' do
        expect { |b| tree1.preorder(&b) }.to yield_control.exactly(9).times
      end

      it 'returns nil' do
        expect(tree1.preorder { |node| node.data }).to be_nil
      end
    end
  end

  describe '#rebalance' do
    context 'when the tree consists of only a root node' do
      it 'does not change the tree' do
        tree = described_class.new([0])
        original_level_order = level_order(tree)
        tree.rebalance
        expect(level_order(tree)).to eq(original_level_order)
      end
    end

    context 'when the tree consists of multiple nodes and is balanced' do
      it 'does not change the tree' do
        original_level_order = level_order(tree1)
        tree1.rebalance
        expect(level_order(tree1)).to eq(original_level_order)
      end
    end

    context 'when the tree consists of multiple nodes and is not balanced' do
      it 'rebalances the tree' do
        tree1.root.right.right.right.right = Tree::Node.new(10)
        tree1.rebalance
        expect(balanced?(tree1)).to be_truthy
      end
    end
  end
end

def balanced?(tree)
  is_balanced = true
  tree.inorder do |node|
    left_height = node.left.nil? ? 0 : tree.height(node.left)
    right_height = node.right.nil? ? 0 : tree.height(node.right)
    is_balanced = false if (left_height - right_height).abs > 1
  end
  is_balanced
end

def level_order(tree)
  data = []
  q = Queue.new
  q.enq(tree.root)
  until q.empty?
    node = q.deq
    data.append(node.data)
    q.enq(node.left) unless node.left.nil?
    q.enq(node.right) unless node.right.nil?
  end
  data
end
