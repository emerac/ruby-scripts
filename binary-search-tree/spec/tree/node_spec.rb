# frozen_string_literal: true

# Test the Node class.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative '../spec_helper'

describe Tree::Node do
  describe '#initialize' do
    context 'when providing no arguments' do
      let(:node) { described_class.new }

      it 'sets the data attribute to nil' do
        expect(node.data).to be_nil
      end

      it 'sets the left attribute to nil' do
        expect(node.left).to be_nil
      end

      it 'sets the right attribute to nil' do
        expect(node.right).to be_nil
      end
    end

    context 'when providing arguments' do
      let(:node) { described_class.new(0, 1, 2) }

      it 'sets the data attribute to the given value' do
        expect(node.data).to eq(0)
      end

      it 'sets the left attribute to the given value' do
        expect(node.left).to eq(1)
      end

      it 'sets the right attribute to the given value' do
        expect(node.right).to eq(2)
      end
    end
  end

  describe '#<=>' do
    let(:lesser_node) { described_class.new(0) }

    let(:greater_node) { described_class.new(1) }

    it 'compares two nodes with < and returns the correct boolean' do
      expect(lesser_node < greater_node).to be_truthy
    end

    it 'compares two nodes with > and returns the correct boolean' do
      expect(lesser_node > greater_node).to be_falsy
    end

    it 'compares two nodes with <= and returns the correct boolean' do
      expect(lesser_node <= greater_node).to be_truthy
    end

    it 'compares two nodes with >= and returns the correct boolean' do
      expect(lesser_node >= greater_node).to be_falsy
    end

    it 'compares two nodes with == and returns the correct boolean' do
      expect(lesser_node == greater_node).to be_falsy
    end

    it 'compares two nodes with <=> and returns the correct boolean' do
      expect(lesser_node <=> greater_node).to eq(-1)
    end
  end

  describe '#inspect' do
    it 'displays node information in a easily-readable format' do
      expect(described_class.new(0).inspect).to match(/.*D.*L.*R/)
    end
  end
end
