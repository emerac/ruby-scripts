# binary search tree

This project implements a binary search tree. This implementation
allows:

* a balanced binary search tree to be built from an array of integers
* nodes to be inserted
    * can be done both iteratively and recursively
* nodes to be deleted
* nodes to be found
* breadth-first level order traversal
    * can be done both iteratively and recursively
    * if given a block, these methods yield a node
    * if no block is given, these methods return an array of values
* depth-first preorder, inorder, and postorder traversal
    * if given a block, these methods yield a node
    * if no block is given, these methods return an array of values
* tree height to be found
* node depth to be found
* the tree to be checked for balance
* the tree to be rebalanced

## Usage

A demonstration script has been provided, which will demonstrate most of
the features described above. A console script has also been provided,
which allows you to experiment with the binary search tree inside an irb
session. To use these scripts:

* clone the `ruby-scripts` repository
* `cd ruby-scripts/binary-search-tree`
* `bin/demo` or `bin/console`

If the last command produces a permission error, it is likely because
the script is not executable. You can change this with the command
`chmod u+x bin/demo` or `chmod u+x bin/console`.

## License

This program is licensed under the GNU General Public License. For
the full license text, view the [LICENSE](../LICENSE) file.
