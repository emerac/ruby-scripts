# frozen_string_literal: true

# Binary search trees.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

module Tree
  # Represents a binary search tree.
  class Tree
    attr_reader :root

    def initialize(source)
      raise ArgumentError unless valid_source?(source)

      @arr = source
      @mod_arr = @arr.uniq.sort
      @root = build_tree(@mod_arr, 0, @mod_arr.length - 1)
    end

    def balanced?
      is_balanced = true
      inorder do |node|
        left_height = node.left.nil? ? 0 : height(node.left)
        right_height = node.right.nil? ? 0 : height(node.right)
        is_balanced = false if (left_height - right_height).abs > 1
      end
      is_balanced
    end

    def delete(val, node = @root)
      raise ArgumentError unless val.instance_of?(Integer)

      return if node.nil?

      # Search for the node.
      if val < node.data
        node.left = delete(val, node.left)
      elsif val > node.data
        node.right = delete(val, node.right)
      # Node found.
      # Node has no children.
      elsif node.left.nil? && node.right.nil?
        node = nil
      # Node has one child.
      elsif node.right.nil?
        node = node.left
      elsif node.left.nil?
        node = node.right
      # Node has two children.
      else
        node.data = find_minimum(node.right).data
        node.right = delete(node.data, node.right)
      end
      node
    end

    def display(node = @root, padding = 0)
      display(node.right, padding + 10) unless node.right.nil?
      puts "#{'-' * padding}#{node.data}"
      display(node.left, padding + 10) unless node.left.nil?
    end

    def depth(val, node = @root)
      raise ArgumentError unless val.instance_of?(Integer)

      return unless contains?(val)

      if val < node.data
        1 + depth(val, node.left)
      elsif val > node.data
        1 + depth(val, node.right)
      else
        0
      end
    end

    def find(val)
      return unless contains?(val)

      node = @root
      node = val < node.data ? node.left : node.right until node.data == val
      node
    end

    def height(node = @root)
      return 0 if node.left.nil? && node.right.nil?

      if !node.left.nil? && !node.right.nil?
        left_depth = 1 + height(node.left)
        right_depth = 1 + height(node.right)
        left_depth >= right_depth ? left_depth : right_depth
      elsif node.right.nil?
        1 + height(node.left)
      elsif node.left.nil?
        1 + height(node.right)
      end
    end

    def inorder(node = @root, data = [], &block)
      inorder(node.left, data, &block) unless node.left.nil?
      block_given? ? block.call(node) : data.append(node.data)
      inorder(node.right, data, &block) unless node.right.nil?
      data unless block_given?
    end

    def insert_iterative(val)
      raise ArgumentError unless val.instance_of?(Integer)

      return if contains?(val)

      node = find_insert_node(val)
      if val < node.data
        node.left = Node.new(val)
      else
        node.right = Node.new(val)
      end
    end

    def insert_recursive(val, node = @root)
      raise ArgumentError unless val.instance_of?(Integer)

      if val < node.data && !node.left.nil?
        insert_recursive(val, node.left)
      elsif val > node.data && !node.right.nil?
        insert_recursive(val, node.right)
      elsif val < node.data
        node.left = Node.new(val)
      elsif val > node.data
        node.right = Node.new(val)
      end
    end

    def level_order_iterative
      data = []
      q = Queue.new
      q.enq(@root)
      until q.empty?
        node = q.deq
        block_given? ? yield(node) : data.append(node.data)
        q.enq(node.left) unless node.left.nil?
        q.enq(node.right) unless node.right.nil?
      end
      data unless block_given?
    end

    def level_order_recursive(nodes = [@root], data = [], &block)
      return data if nodes.empty?

      node = nodes.shift

      nodes.append(node.left) unless node.left.nil?
      nodes.append(node.right) unless node.right.nil?

      block_given? ? block.call(node) : data.append(node.data)

      level_order_recursive(nodes, data, &block)
      data unless block_given?
    end

    def postorder(node = @root, data = [], &block)
      postorder(node.left, data, &block) unless node.left.nil?
      postorder(node.right, data, &block) unless node.right.nil?
      block_given? ? block.call(node) : data.append(node.data)
      data unless block_given?
    end

    def preorder(node = @root, data = [], &block)
      block_given? ? block.call(node) : data.append(node.data)
      preorder(node.left, data, &block) unless node.left.nil?
      preorder(node.right, data, &block) unless node.right.nil?
      data unless block_given?
    end

    def rebalance
      values = inorder
      @root = build_tree(values, 0, values.length - 1)
    end

    private

    def build_tree(arr, first, last)
      return nil if first > last

      mid = (first + last) / 2
      root = Node.new(arr[mid])
      root.left = build_tree(arr, first, mid - 1)
      root.right = build_tree(arr, mid + 1, last)
      root
    end

    def contains?(val)
      !@root.nil? && level_order_iterative.include?(val)
    end

    def find_insert_node(value)
      node = @root
      until (value < node.data && node.left.nil?) ||
            (value > node.data && node.right.nil?)
        node = value < node.data ? node.left : node.right
      end
      node
    end

    def find_minimum(node = @root)
      node = node.left until node.left.nil?
      node
    end

    def valid_source?(source)
      source.instance_of?(Array) && !source.empty? && source.all?(Integer)
    end
  end
end
