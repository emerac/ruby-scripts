# frozen_string_literal: true

# Nodes for binary search trees.
#
# Copyright (C) 2021 emerac
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

module Tree
  # Represents a node in a binary search tree.
  class Node
    include Comparable
    attr_accessor :data, :left, :right

    def initialize(data = nil, left = nil, right = nil)
      @data = data
      @left = left
      @right = right
    end

    def <=>(other)
      data <=> other.data
    end

    def inspect
      left_string = @left.nil? ? 'x' : @left.data
      right_string = @right.nil? ? 'x' : @right.data
      "(D:#{@data}, L:#{left_string}, R:#{right_string})"
    end
  end
end
